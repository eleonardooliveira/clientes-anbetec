const express = require( 'express' );
const router = express.Router();
const bcrypt = require( 'bcryptjs' );
const config = require( 'config' );
const jwt = require( 'jsonwebtoken' );
const auth = require( '../../middleware/auth' );
const MailService = require( '@sendgrid/mail' );

//Administrador Model
const Administrador = require( '../../models/Administrador' );
//Parceiro Model
const Parceiro = require( '../../models/Parceiro' );

// @route   POST api/auth/administrador
// @desc    Loga administrador
// @acess   Public
router.post( '/administrador', ( req, res ) => {
    const { email, senha } = req.body;

    //Validações simples
    if ( !email || !senha ) {
        return res.status( 400 ).json( { msg: 'Por favor, insira todos os dados!' } );
    }

    //Verificar usuário já cadastrado
    Administrador.findOne( { email } ).then( administrador => {
        if ( !administrador ) return res.status( 400 ).json( { msg: 'Credenciais inválidas!' } );

        //Validando a senha
        bcrypt.compare( senha, administrador.senha )
            .then( isMatch => {

                if ( !isMatch ) return res.status( 400 ).json( { msg: 'Credenciais inválidas!' } );

                jwt.sign(
                    {
                        id: administrador.id,
                        type: "admin"
                    },
                    config.get( 'jwtSecret' ),
                    { expiresIn: 3600 },
                    ( err, token ) => {
                        if ( err ) throw err;

                        res.json( {
                            token,
                            administrador: {
                                id: administrador.id,
                                nomeAdministrador: administrador.nomeAdministrador,
                                email: administrador.email,
                                isAtivo: administrador.isAtivo
                            }
                        } );
                    }
                );
            } );
    } );
} );

// @route   GET api/auth/administradorInfos
// @desc    Retorna infos do usuário
// @acess   Private
router.get( '/administradorInfos', auth, ( req, res ) => {
    if ( req.data.type === 'admin' ) {
        Administrador.findById( req.data.id )
            .select( '--senha' )
            .then( administrador => res.json( administrador ) );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   POST api/auth/parceiro
// @desc    Loga parceiro
// @acess   Public
router.post( '/parceiro', ( req, res ) => {
    const { email, senha } = req.body;

    //Validações simples
    if ( !email || !senha ) {
        return res.status( 400 ).json( { msg: 'Por favor, insira todos os dados!' } );
    }

    //Verificar usuário já cadastrado
    Parceiro.findOne( { email } ).then( parceiro => {
        if ( !parceiro ) return res.status( 400 ).json( { msg: 'Credenciais inválidas!' } );

        //Validando a senha
        bcrypt.compare( senha, parceiro.senha )
            .then( isMatch => {

                if ( !isMatch ) return res.status( 400 ).json( { msg: 'Credenciais inválidas!' } );

                jwt.sign(
                    {
                        id: parceiro.id,
                        type: "parc"
                    },
                    config.get( 'jwtSecret' ),
                    { expiresIn: 3600 },
                    ( err, token ) => {
                        if ( err ) throw err;
                        res.json( {
                            token,
                            parceiro: {
                                id: parceiro.id,
                                nomeParceiro: parceiro.nomeParceiro,
                                email: parceiro.email,
                                isAtivo: parceiro.isAtivo
                            }
                        } );
                    }
                );
            } );
    } );
} );

// @route   GET api/auth/parceiroInfos
// @desc    Retorna infos do usuário
// @acess   Private
router.get( '/parceiroInfos', auth, ( req, res ) => {
    if ( req.data.type === 'parc' ) {
        Parceiro.findById( req.data.id )
            .select( '--senha' )
            .then( parceiro => res.json( parceiro ) );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   POST api/auth/cliete
// @desc    Loga cliente
// @acess   Public
router.post( '/cliente', ( req, res ) => {
    const { email, senha } = req.body;

    //Validações simples
    if ( !email || !senha ) {
        return res.status( 400 ).json( { msg: 'Por favor, insira todos os dados!' } );
    }

    //Verificar cliente já cadastrado
    Cliente.findOne( { email } ).then( cliente => {
        if ( !cliente ) return res.status( 400 ).json( { msg: 'Credenciais inválidas!' } );

        //Validando a senha
        bcrypt.compare( senha, cliente.senha )
            .then( isMatch => {

                if ( !isMatch ) return res.status( 400 ).json( { msg: 'Credenciais inválidas!' } );

                jwt.sign(
                    {
                        id: cliente.id,
                        type: "client"
                    },
                    config.get( 'jwtSecret' ),
                    { expiresIn: 3600 },
                    ( err, token ) => {
                        if ( err ) throw err;

                        res.json( {
                            token,
                            cliente: {
                                id: cliente.id,
                                nomeCliente: cliente.nomeCliente,
                                email: cliente.email,
                                isAtivo: cliente.isAtivo
                            }
                        } );
                    }
                );
            } );
    } );
} );

// @route   GET api/auth/clienteInfos
// @desc    Retorna infos do usuário
// @acess   Private
router.get( '/clienteInfos', auth, ( req, res ) => {
    if ( req.data.type === 'client' ) {
        Cliente.findById( req.data.id )
            .select( '--senha' )
            .then( cliente => res.json( cliente ) );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   PUT api/auth/recuperarSenha
// @desc    Cria um código de autenticação e envia por e-mail
// @acess   Public
router.put( '/recuperarSenha', ( req, res ) => {
    const { email } = req.body;

    Parceiro.findOne( { email: email } ).then( parceiro => {
        if ( parceiro ) {
            var codigoRecuperacaoSenha = Math.floor( Math.random() * 90000 ) + 10000;

            sendEmail( codigoRecuperacaoSenha, email );

            Parceiro.findOneAndUpdate(
                { email: email },
                {
                    codigoRecuperacaoSenha,
                    dataRecuperacaoSenha: new Date()
                },
                { new: true },
                function ( err, response ) {
                    if ( err ) { res.status( 404 ).json( { sucess: true } ); }

                    res.json( { sucess: true } );
                } );
        } else {
            res.json( { sucess: true } );
        }

    } );
} );

async function sendEmail( codigo, destinatario ) {
    const mail = {
        to: destinatario,
        from: 'eleonardo.ro@gmail.com',
        subject: 'Código de recuperação de senha',
        html: `Use o código abaixo para recuperar sua senha!<br/><h3> ${ codigo }</h3>`
    };
    MailService.setApiKey( config.get( 'sendGridAPIKey' ) );
    await MailService.send( mail );
};

// @route   PUT api/auth/validarCodigoRecuperarSenha
// @desc    Valida o código de recuperação que foi enviada
// @acess   Public
router.put( '/validarCodigoRecuperarSenha', ( req, res ) => {
    const { email, codigo } = req.body;

    Parceiro.findOne( { email } ).then( parceiro => {//Encontrando parceiro com esse código
        const agora = parceiro.dataRecuperacaoSenha.getTime();
        const dataCodigo = new Date().getTime();

        //validando se codigo é válido
        if ( parceiro.codigoRecuperacaoSenha === codigo ) {
            var diff = ( ( dataCodigo - agora ) / 1000 ) / 60;

            if ( diff < 120 ) {//o código está correto e não expirou ainda

                //apagando código
                Parceiro.findOneAndUpdate(
                    { email: email },
                    {
                        codigoRecuperacaoSenha: '',
                        dataRecuperacaoSenha: ''
                    },
                    function ( err, response ) {
                        if ( err ) { res.status( 404 ).json( { sucess: false } ); }

                        res.status( 200 ).json( { sucess: true } );
                    } );


            } else {//código expirado
                return res.status( 400 ).json( { msg: 'Código expirado, solicite um novo!' } );
            }
        } else {//código inválido
            return res.status( 400 ).json( { msg: 'Código inválido!' } );
        }
    } );
} );

// @route   PUT api/auth/alterarSenha
// @desc    Salva nova senha inserida pelo usuário
// @acess   Public
router.put( '/alterarSenha', ( req, res ) => {
    const { email, senha } = req.body;

    Parceiro.findOne( { email } ).then( parceiro => {
        //atualizando usuário com nova senha
        bcrypt.genSalt( 10, ( err, salt ) => {
            bcrypt.hash( senha, salt, ( err, hash ) => {
                if ( err ) throw err;
                Parceiro.findOneAndUpdate(
                    { email: email },
                    {
                        senha: hash
                    },
                    { new: true },
                    function ( err, response ) {
                        if ( err ) { res.status( 404 ).json( { sucess: false } ); }

                        res.status( 200 ).json( { sucess: true } );
                    } );

            } );
        } );
    } );
} );

module.exports = router;