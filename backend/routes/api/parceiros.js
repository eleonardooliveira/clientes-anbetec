const express = require( 'express' );
const router = express.Router();
const bcrypt = require( 'bcryptjs' );
const config = require( 'config' );
const jwt = require( 'jsonwebtoken' );
const auth = require( '../../middleware/auth' );

//Parceiro Model
const Parceiro = require( '../../models/Parceiro' );

// @route   GET api/parceiros/verifyToken
// @desc    Valida se o token enviado é válido e se não está expirado
// @acess   Public
router.get( '/verifyToken', auth, ( req, res ) => {
    if ( req.data.type === 'parc' ) {

        const token = req.header( 'x-auth-token' );

        //Validando se passou token
        if ( !token ) return res.status( 401 ).json( { msg: 'Token não enviado, acesso negado!' } );

        jwt.verify( token, config.get( 'jwtSecret' ), ( err, verifiedJwt ) => {
            if ( err ) {
                return res.status( 401 ).json( { msg: err.message } );
            } else {
                res.send( verifiedJwt );
            }
        } );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   POST api/parceiros
// @desc    Registrar novo parceiro
// @acess   Public
router.post( '/', ( req, res ) => {
    const { nomeParceiro, email, telefone = '99', senha = 123456, rotinas = {}, isAtivo = true } = req.body;

    //Validações simples
    if ( !nomeParceiro ) { return res.status( 400 ).json( { msg: 'Digite o Nome do Parceiro!' } ); }
    if ( !telefone ) { return res.status( 400 ).json( { msg: 'Digite o Telefone do Parceiro!' } ); }
    if ( !email ) { return res.status( 400 ).json( { msg: 'Digite o E-mail do Parceiro!' } ); }
    if ( !rotinas ) { return res.status( 400 ).json( { msg: 'Não foram selecionadas Rotinas, contate um Administrador do Sistema!' } ); }
    if ( !senha ) { return res.status( 400 ).json( { msg: 'Digite uma Senha!' } ); }

    //Verificar parceiro já cadastrado
    Parceiro.findOne( { email } ).then( parceiro => {
        if ( parceiro ) return res.status( 400 ).json( { msg: 'Parceiro já cadastro!' } );

        const novoParceiro = new Parceiro( {
            nomeParceiro,
            telefone,
            email,
            rotinas,
            senha,
            isAtivo
        } );

        //Criando salt & hash
        bcrypt.genSalt( 10, ( err, salt ) => {
            bcrypt.hash( novoParceiro.senha, salt, ( err, hash ) => {
                if ( err ) throw err;
                novoParceiro.senha = hash;
                novoParceiro.save()
                    .then( novoParceiro => {

                        jwt.sign(
                            { id: novoParceiro.id },
                            config.get( 'jwtSecret' ),
                            { expiresIn: 3600 },
                            ( err, token ) => {
                                if ( err ) throw err;

                                res.json( {
                                    status: "OK",
                                    parceiro: {
                                        id: novoParceiro.id,
                                        nomeParceiro: novoParceiro.nomeParceiro,
                                        email: novoParceiro.email,
                                        telefone: novoParceiro.telefone,
                                        rotinas: novoParceiro.rotinas,
                                        isAtivo: false
                                    }
                                } );
                            }
                        );
                    } );
            } );
        } );
    } );
} );

// @route   GET api/auth/donoRotinas
// @desc    Retorna infos do dono da Rotina
// @acess   Private
router.get( '/rotinasParceiroLogado', auth, ( req, res ) => {
    if ( req.data.type === 'parc' || req.data.type === 'admin' ) {
        if ( req.data.id ) {//passou id de um rotina específico
            Rotina.find()
                .where( 'idParceiroDono' ).equals( req.data.id )
                .then( rotina => res.json( rotina ) );
        }
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   GET api/auth/infosParceiroLogado
// @desc    Retorna infos do dono da Rotina
// @acess   Private
router.get( '/infosParceiroLogado', auth, ( req, res ) => {
    if ( req.data.type === 'parc' || req.data.type === 'admin' ) {
        Parceiro.findById( req.data.id )
            .then( parceiro => res.json( parceiro ) );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   GET api/parceiros
// @desc    Retorna todos os parceiros ou apenas um se passar ID
// @acess   Private
router.get( '/:idParceiro?', auth, ( req, res ) => {
    if ( req.data.type === 'parc' || req.data.type === 'admin' ) {
        if ( req.query.idParceiro ) {//passou id de um cliente específico
            Parceiro.findById( req.query.idParceiro )
                .then( parceiro => res.json( parceiro ) );
        } else {//não passou id de um cliente específico
            Parceiro.find()
                .sort( { date: -1 } )
                .then( parceiros => res.json( parceiros ) );
        }
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   DELETE api/parceiros/:id
// @desc    Deleta um parceiro
// @acess   Private
router.delete( '/:id', auth, ( req, res ) => {
    if ( req.data.type === 'parc' || req.data.type === 'admin' ) {
        if ( !req.params.id ) return res.status( 400 ).json( { msg: 'Passe um id!' } );

        Parceiro.findById( req.params.id )
            .then( parceiro => {
                parceiro.remove().then( () => res.json( { sucess: true } ) );
            } )
            .catch( err => res.status( 404 ).json( { sucess: false } ) );

    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   PUT api/parceiros/:id
// @desc    Atualiza um parceiro
// @acess   Private
router.put( '/atualizarProprio', ( req, res ) => {
    const { id, nomeParceiro, telefone, changingPassword, senhaAntiga, senhaNova = {} } = req.body;

    if ( !nomeParceiro ) { return res.status( 400 ).json( { msg: 'Digite o Nome!' } ); }
    if ( !telefone ) { return res.status( 400 ).json( { msg: 'Digite o Telefone!' } ); }
    if ( !senhaAntiga !== !senhaNova ) {//se tiver preenchido apenas um dos campos de senha
        return res.status( 400 ).json( { msg: 'Para alterar a senha é preciso digitar a Senha Atual e a Senha Nova!' } );
    }

    if ( senhaAntiga.trim().length < 6 ) {
        return res.status( 400 ).json( { msg: 'Senha Antiga inválida, digite pelo menos 6 caracteres!' } );
    }

    if ( senhaNova.trim().length < 6 ) {
        return res.status( 400 ).json( { msg: 'Senha Nova inválida, digite pelo menos 6 caracteres!' } );
    }

    if ( changingPassword === true && senhaAntiga && senhaNova ) { //está alterando a senha

        //Verificar usuário já cadastrado
        Parceiro.findById( id ).then( parceiro => {

            //Validando a senha
            bcrypt.genSalt( 10, ( err, salt ) => {
                bcrypt.compare( senhaAntiga, parceiro.senha )
                    .then( isMatch => {//a senha digitada está correta

                        if ( !isMatch ) return res.status( 400 ).json( { msg: 'A senha digitada não está correta!' } );

                        bcrypt.hash( senhaNova, salt, ( err, hash ) => {//criando nova senha excriptada
                            if ( err ) throw err;

                            //atualizando os dados
                            Parceiro.findByIdAndUpdate(
                                id,
                                {
                                    nomeParceiro: req.body.nomeParceiro,
                                    telefone: req.body.telefone,
                                    senha: hash
                                },
                                { new: true },
                                function ( err, response ) {
                                    // Handle any possible database errors
                                    if ( err ) {
                                        res.status( 404 ).json( { sucess: false } );
                                    }
                                    res.json( { response } );
                                } );

                        } );

                    } );
            } );
        } );
    }
} );

// @route   PUT api/parceiros/:id
// @desc    Atualiza um parceiro
// @acess   Private
router.put( '/', ( req, res ) => {
    const { nomeParceiro, telefone, email, rotinas = {} } = req.body;

    if ( !nomeParceiro ) { return res.status( 400 ).json( { msg: 'Digite o Nome!' } ); }
    if ( !telefone ) { return res.status( 400 ).json( { msg: 'Digite o Telefone!' } ); }
    if ( !rotinas ) { return res.status( 400 ).json( { msg: 'Não foram selecionadas Rotinas, contate um Administrador do Sistema!' } ); }

    Parceiro.findByIdAndUpdate(
        req.body.id,
        {
            nomeParceiro: req.body.nomeParceiro,
            telefone: req.body.telefone,
            rotinas: req.body.rotinas,
            isAtivo: req.body.isAtivo
        },
        { new: true },
        function ( err, response ) {
            // Handle any possible database errors
            if ( err ) {
                res.status( 404 ).json( { sucess: false } );
            }
            res.json( { response } );
        } );
} );

module.exports = router;