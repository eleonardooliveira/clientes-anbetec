const express = require( 'express' );
const router = express.Router();
const auth = require( '../../middleware/auth' );
const MailService = require( '@sendgrid/mail' );
const config = require( 'config' );

const selectFields = 'nomeRotina descricaoRotina descricaoCompletaRotina modulo vendaMensal valorMensal vendaVitalicio valorVitalicio';
const filterActives = { isAtivo: 'true' };

//Rotina Model
const Rotina = require( '../../models/Rotina' );

// @route   GET api/rotinas/donoInfos
// @desc    Retorna infos do dono da Rotina
// @acess   Private
router.get( '/donoInfos', auth, ( req, res ) => {
    const idParceiroDono = req.query.idParceiroDono;

    Parceiro.findById( idParceiroDono )
        .select( '--senha' )
        .then(
            parceiro => {
                if ( parceiro !== null )//encontrou parceiro
                    res.json( parceiro );
                else {//não encontrou, o dono é o admin
                    Administrador.findById( idParceiroDono )
                        .select( '--senha' )
                        .then( administrador => { res.json( administrador ); } );
                }
            } );
} );

// @route   GET api/rotinas/modulos
// @desc    Retorna Módulos que tem Rotinas cadastrados independente do dono
// @acess   Private
router.get( '/modulos', ( req, res ) => {

    Rotina.find()
        .sort( { date: -1 } )
        .select( 'modulo -_id' )
        .then( rotinas => {
            var uniqueArray = [];

            rotinas.map( function ( element ) {
                if ( uniqueArray.indexOf( element.modulo ) === -1 )
                    uniqueArray.push( element.modulo );
            } );

            res.json( uniqueArray );
        } );
} );

// @route   GET api/rotinas/modulosAnbetec
// @desc    Retorna Módulos que tem Rotinas cadastrados e são da Anbetec
// @acess   Private
router.get( '/modulosAnbetec', ( req, res ) => {

    Rotina.find( { 'idParceiroDono': '5e4436d86dabbb55a46d5a6c' } )
        .sort( { date: -1 } )
        .select( 'modulo -_id' )
        .then( rotinas => {
            var uniqueArray = [];

            rotinas.map( function ( element ) {
                if ( uniqueArray.indexOf( element.modulo ) === -1 )
                    uniqueArray.push( element.modulo );
            } );

            res.json( uniqueArray );
        } );
} );

// @route   GET api/rotinas/modulosParceiros
// @desc    Retorna Módulos que tem Rotinas cadastrados e não são da Anbetec
// @acess   Private
router.get( '/modulosParceiros', ( req, res ) => {

    Rotina.find( { 'idParceiroDono': { '$ne': '5e4436d86dabbb55a46d5a6c' } } )
        .sort( { date: -1 } )
        .select( 'modulo -_id' )
        .then( rotinas => {
            var uniqueArray = [];

            rotinas.map( function ( element ) {
                if ( uniqueArray.indexOf( element.modulo ) === -1 )
                    uniqueArray.push( element.modulo );
            } );

            res.json( uniqueArray );
        } );
} );

function mountSearchQuery( termoPesquisa ) {
    if ( termoPesquisa ) {
        return {
            $and: [
                { 'nomeRotina': { "$regex": termoPesquisa.replace( /[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&" ), "$options": "i" } },
                { 'descricaoRotina': { "$regex": termoPesquisa.replace( /[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&" ), "$options": "i" } },
                { 'descricaoCompletaRotina': { "$regex": termoPesquisa.replace( /[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&" ), "$options": "i" } },
            ]
        };
    } else {
        return {};
    }
}

function selectOrder( ordenar ) {
    var ordenarAux;
    switch ( ordenar ) {
        case 'Relevância':
            ordenarAux = '-countClicks';
            break;
        case 'Novas primeiro':
            ordenarAux = '-data';
            break;
        case 'Antigas primeiro':
            ordenarAux = '+data';
            break;
        case 'Menor valor Mensal primeiro':
            ordenarAux = 'valorMensal';
            break;
        case 'Maior valor Mensal primeiro':
            ordenarAux = '-valorMensal';
            break;
        case 'Menor valor Vitalício primeiro':
            ordenarAux = 'valorVitalicio';
            break;
        case 'Maior valor Vitalício primeiro':
            ordenarAux = '-valorVitalicio';
            break;
        default:
            ordenarAux = '-countClicks';
            break;
    }

    return ordenarAux;
}

function selectFilter( filtrar ) {
    var filterAux = {};
    if ( filtrar === 'Apenas Venda Mensal' )
        filterAux = { isAtivo: 'true', vendaVitalicio: 'false' };
    else if ( filtrar === 'Apenas Venda Vitalícia' )
        filterAux = { isAtivo: 'true', vendaMensal: 'false' };

    return filterAux;
}

function selectModule( modulo ) {
    var moduleAux = {};
    if ( modulo === 'Todas Anbetec' ) {
        moduleAux = { 'idParceiroDono': '5e4436d86dabbb55a46d5a6c' };
    } else if ( modulo === 'Todas de Parceiros' ) {
        moduleAux = { 'idParceiroDono': { '$ne': '5e4436d86dabbb55a46d5a6c' } };
    } else if ( modulo !== "Todas" ) {
        moduleAux = { 'modulo': modulo };
    }

    return moduleAux;
}

function selectDono( dono ) {
    var donoAux = {};

    if ( dono === "Anbetec" ) {
        donoAux = { 'idParceiroDono': '5e4436d86dabbb55a46d5a6c' };
    } else if ( dono === "Parceiros" ) {
        donoAux = { 'idParceiroDono': { '$ne': '5e4436d86dabbb55a46d5a6c' } };
    }

    return donoAux;
}

// @route   GET api/rotinas/getTodasRotinasVitrine
// @desc    Retorna Todas as Rotinas Ativas para serem exibidas na Vitrine
// @acess   Public
router.get( '/getTodasRotinasVitrine', ( req, res ) => {
    Rotina.find( { "isAtivo": "true" } )
        .sort( "-countClicks" )
        .then( rotinas => res.json( rotinas ) );
} );

// @route   GET api/rotinas/getRotinasVitrine
// @desc    Retorna Rotinas para serem exibidas na Vitrine
// @acess   Public
router.get( '/getRotinasVitrine/:ordenar?/:filtrar?/:termoPesquisa?/:modulo?', ( req, res ) => {
    const { termoPesquisa, dono, ordenar, filtrar, modulo } = req.query;

    const searchAux = mountSearchQuery( termoPesquisa );
    const ordenarAux = selectOrder( ordenar );
    const filterAux = selectFilter( filtrar );
    const moduleAux = selectModule( modulo );
    const donoAux = selectDono( dono );

    console.log( '\\/\\/\\/\\/\\/' );
    console.log( 'termoPesquisa: [' + termoPesquisa + ']' );
    console.log( 'ordenar: [' + ordenar + ']' );
    console.log( 'filtrar: [' + filtrar + ']' );
    console.log( 'modulo: [' + modulo + ']' );
    console.log( 'dono: [' + dono + ']' );
    console.log( '-----------------' );
    console.log( 'searchAux: [' + JSON.stringify( searchAux ) + ']' );
    console.log( 'ordenarAux: [' + JSON.stringify( ordenarAux ) + ']' );
    console.log( 'filterAux: [' + JSON.stringify( filterAux ) + ']' );
    console.log( 'moduleAux: [' + JSON.stringify( moduleAux ) + ']' );
    console.log( 'donoAux: [' + JSON.stringify( donoAux ) + ']' );
    const findComplete = { ...filterActives, ...filterAux, ...moduleAux, ...searchAux, ...donoAux };
    console.log( "findComplete: [" + JSON.stringify( findComplete ) + "]" );
    console.log( '/\\/\\/\\/\\/\\/\\' );

    Rotina.find( findComplete )
        .sort( ordenarAux )
        .select( selectFields )
        .collation( { locale: "en_US", numericOrdering: true } )
        .then( rotinas => {
            return res.json( rotinas );
        } );

} );

// @route   GET api/rotinas
// @desc    Retorna todas as rotinas, apenas uma se passar ID ou apenas rotinas ativas
// @acess   Private
router.get( '/:idRotina?/:ativas?/:idParceiro?', auth, ( req, res ) => {

    if ( req.data.type === 'parc' || req.data.type === 'admin' ) {
        if ( req.query.idRotina ) {//passou id de um rotina específico
            Rotina.findById( req.query.idRotina )
                .then( rotina => res.json( rotina ) );
        } else if ( req.query.ativas ) {//quer apenas as rotinas ativas
            Rotina.find()
                .where( 'isAtivo' ).equals( 'true' )
                .then( rotina => res.json( rotina ) );
        } else if ( req.query.idParceiro ) {//quer apenas as rotinas ativas
            Rotina.find()
                .where( 'idParceiroDono' ).equals( req.query.idParceiro )
                .then( rotinas => res.json( rotinas ) );
        } else {//não passou id de um rotina específico
            Rotina.find()
                .sort( { date: -1 } )
                .then( rotinas => res.json( rotinas ) );
        }
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

function incrementValue( idRotina, valueAux, res ) {
    Rotina.findByIdAndUpdate(
        idRotina,
        { $inc: { [valueAux]: 1 } },
        function ( err, response ) {
            if ( err ) {
                res.status( 404 ).json( { sucess: false } );
            }
            res.json( { response } );
        } );
}

// @route   PUT api/rotinas/seeMore
// @desc    Increments count of clicks in 'See More' button of a Routine in Routines showcase
// @acess   Public
router.put( '/seeMore', ( req, res ) => {
    const { idRotina } = req.body;
    incrementValue( idRotina, 'countClicks', res );
} );

// @route   PUT api/rotinas/emailInteresse
// @desc    Envia um e-mail para o comercial com notificação de cadastro em uma Rotina
// @acess   Public
router.put( '/emailInteresse', ( req, res ) => {
    const { nome, telefone, nomeRotina, idRotina } = req.body;
    sendEmail( nome, telefone, nomeRotina );
    incrementValue( idRotina, 'countInterests', res );
} );

async function sendEmail( nome, telefone, nomeRotina ) {
    const mail = {
        to: 'eleonardo.oliveira@anbetec.com.br',
        from: 'eleonardo.ro@gmail.com',
        subject: 'Cadastro de Interesse em uma Rotina',
        html: `O cliente <b>${ nome }</b> cadastrou interesse na Rotina <b>${ nomeRotina }</b>. <br />Você pode entrar em contato com ele pelo telefone <b>${ telefone }</b>`
    };
    MailService.setApiKey( config.get( 'sendGridAPIKey' ) );
    await MailService.send( mail );
};

// @route   POST api/rotinas
// @desc    Cria uma rotina
// @acess   Private
router.post( '/', auth, ( req, res ) => {

    if ( !req.body.nomeRotina ) return res.status( 400 ).json( { msg: 'Digite o Nome do Rotina!' } );
    if ( !req.body.descricaoRotina ) return res.status( 400 ).json( { msg: 'Digite uma Descrição para a Rotina!' } );
    if ( !req.body.descricaoCompletaRotina ) return res.status( 400 ).json( { msg: 'Digite uma Descrição para a Rotina!' } );
    if ( !req.body.modulo ) return res.status( 400 ).json( { msg: 'Selecione um Módulo para a Rotina!' } );
    if ( !req.body.vendaMensal && !req.body.vendaVitalicio ) return res.status( 400 ).json( { msg: 'Selecione uma modalidade de venda para a Rotina!' } );
    if ( req.body.vendaMensal && !req.body.valorMensal ) return res.status( 400 ).json( { msg: 'Digite o Valor Mensal da Rotina!' } );
    if ( req.body.vendaVitalicio && !req.body.valorVitalicio ) return res.status( 400 ).json( { msg: 'Digite o Valor Vitalício Rotina!' } );

    const newRotina = new Rotina( {
        nomeRotina: req.body.nomeRotina,
        descricaoRotina: req.body.descricaoRotina,
        descricaoCompletaRotina: req.body.descricaoCompletaRotina,
        modulo: req.body.modulo,
        vendaMensal: req.body.vendaMensal,
        vendaVitalicio: req.body.vendaVitalicio,
        valorMensal: Number.parseFloat( req.body.valorMensal ),
        valorVitalicio: Number.parseFloat( req.body.valorVitalicio ),
        idParceiroDono: req.data.id,
        isAtivo: req.body.isAtivo
    } );


    newRotina.save().then( rotina => {
        res.json( rotina );
    } );
} );


// @route   PUT api/rotinas/:id
// @desc    Atualiza um rotina
// @acess   Private
router.put( '/', ( req, res ) => {

    if ( !req.body.nomeRotina ) return res.status( 400 ).json( { msg: 'Digite o Nome do Rotina!' } );
    if ( !req.body.descricaoRotina ) return res.status( 400 ).json( { msg: 'Digite uma Descrição para a Rotina!' } );
    if ( !req.body.descricaoCompletaRotina ) return res.status( 400 ).json( { msg: 'Digite uma Descrição para a Rotina!' } );
    if ( !req.body.modulo ) return res.status( 400 ).json( { msg: 'Selecione um Módulo para a Rotina!' } );
    if ( !req.body.vendaMensal && !req.body.vendaVitalicio ) return res.status( 400 ).json( { msg: 'Selecione uma modalidade de venda para a Rotina!' } );
    if ( req.body.vendaMensal && !req.body.valorMensal ) return res.status( 400 ).json( { msg: 'Digite o Valor Mensal da Rotina!' } );
    if ( req.body.vendaVitalicio && !req.body.valorVitalicio ) return res.status( 400 ).json( { msg: 'Digite o Valor Vitalício Rotina!' } );

    Rotina.findByIdAndUpdate(
        req.body.id,
        {
            nomeRotina: req.body.nomeRotina,
            descricaoRotina: req.body.descricaoRotina,
            descricaoCompletaRotina: req.body.descricaoCompletaRotina,
            modulo: req.body.modulo,
            vendaMensal: req.body.vendaMensal,
            vendaVitalicio: req.body.vendaVitalicio,
            valorMensal: Number.parseFloat( req.body.valorMensal ),
            valorVitalicio: Number.parseFloat( req.body.valorVitalicio ),
            isAtivo: req.body.isAtivo
        },
        { new: true },
        function ( err, response ) {
            // Handle any possible database errors
            if ( err ) {
                res.status( 404 ).json( { sucess: false } );
            }
            res.json( { response } );
        } );

} );

module.exports = router;