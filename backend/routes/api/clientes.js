const express = require( 'express' );
const router = express.Router();
const jwt = require( 'jsonwebtoken' );
const bcrypt = require( 'bcryptjs' );
const config = require( 'config' );
const auth = require( '../../middleware/auth' );

//Cliente Model
const Cliente = require( '../../models/Cliente' );

// @route   GET api/clientes/verifyToken
// @desc    Valida se o token enviado é válido e se não está expirado
// @acess   Public
router.get( '/verifyToken', auth, ( req, res ) => {
    if ( req.data.type === 'client' ) {

        const token = req.header( 'x-auth-token' );

        //Validando se passou token
        if ( !token ) return res.status( 401 ).json( { msg: 'Token não enviado, acesso negado!' } );

        jwt.verify( token, config.get( 'jwtSecret' ), ( err, verifiedJwt ) => {
            if ( err ) {
                return res.status( 401 ).json( { msg: err.message } );
            } else {
                res.send( verifiedJwt );
            }
        } );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   GET api/clientes/donoInfos
// @desc    Retorna se o CLIENTE tem acesso à ROTINA
// @acess   Public
router.get( '/temAcesso', ( req, res ) => {

    if ( !req.query.codigoClientePC ) return res.status( 400 ).json( { msg: 'Envie o Id do Cliente!' } );
    if ( !req.query.idRotina ) return res.status( 400 ).json( { msg: 'Envie o Id da Rotina!' } );

    const codigoClientePC = req.query.codigoClientePC;
    const idRotina = req.query.idRotina;


    Cliente.findOne()
        .where( 'codigoClientePC' ).equals( codigoClientePC )
        .then( cliente => {
            if ( !cliente )
                return res.json( { temAcesso: false } );

            //cliente está ativa? Se não está ativo, não tem acesso algum
            if ( !cliente.isAtivo )
                return res.json( { temAcesso: false } );

            //cliente é AMS?      Se for tem acesso
            if ( cliente.isAMS )
                return res.json( { temAcesso: true } );

            //cliente tem acesso a essa Rotina específica?
            res.json( { temAcesso: cliente.rotinas.includes( idRotina ) } );
        }
        );
} );

// @route   GET api/clientes
// @desc    Retorna todos os clientes ou apenas um se passar ID
// @acess   Private
router.get( '/:idCliente?', auth, ( req, res ) => {
    if ( req.data.type === 'client' || req.data.type === 'admin' ) {
        if ( req.query.idCliente ) {//passou id de um cliente específico
            Cliente.findById( req.query.idCliente )
                .then( cliente => res.json( cliente ) );
        } else {//não passou id de um cliente específico
            Cliente.find()
                .sort( { date: -1 } )
                .then( clientes => res.json( clientes ) );
        }
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   POST api/clientes
// @desc    Cria um cliente. Usada quando o cliente mesmo se registra, logo, temos todos os dados
// @acess   Private
router.post( '/', ( req, res ) => {
    const { nomeCliente, codigoClientePC, email, telefone, rotinas, senha = 232, isAtivo, isAMS } = req.body;

    //Validações simples
    if ( !nomeCliente ) {
        return res.status( 400 ).json( { msg: 'Digite o Nome do Cliente!' } );
    }
    if ( !codigoClientePC ) {
        return res.status( 400 ).json( { msg: 'Digite o Código do Cliente!' } );
    }
    if ( !email ) {
        return res.status( 400 ).json( { msg: 'Digite o E-mail do Cliente!' } );
    }
    if ( !telefone ) {
        return res.status( 400 ).json( { msg: 'Digite o Telefone do Cliente!' } );
    }
    if ( !senha ) {
        return res.status( 400 ).json( { msg: 'Digite uma Senha!' } );
    }
    if ( !rotinas ) {
        return res.status( 400 ).json( { msg: 'Não foram selecionadas Rotinas, contate um Administrador do Sistema!' } );
    }

    //Verificar cliente já cadastrado
    Cliente.findOne( { email } ).then( cliente => {
        if ( cliente ) return res.status( 400 ).json( { msg: 'Cliente já cadastro!' } );

        const novoCliente = new Cliente( {
            nomeCliente,
            codigoClientePC,
            email,
            telefone,
            rotinas,
            senha,
            isAtivo,
            isAMS
        } );

        //Criando salt & hash
        bcrypt.genSalt( 10, ( err, salt ) => {
            bcrypt.hash( novoCliente.senha, salt, ( err, hash ) => {
                if ( err ) throw err;
                novoCliente.senha = hash;
                novoCliente.save()
                    .then( novoCliente => {

                        jwt.sign(
                            { id: novoCliente.id },
                            config.get( 'jwtSecret' ),
                            { expiresIn: 3600 },
                            ( err, token ) => {
                                if ( err ) throw err;

                                res.json( {
                                    status: "OK",
                                    cliente: {
                                        id: novoCliente.id,
                                        nomeCliente: novoCliente.nomeCliente,
                                        codigoClientePC: novoCliente.codigoClientePC,
                                        email: novoCliente.email,
                                        telefone: novoCliente.telefone,
                                        rotinas: novoCliente.rotinas,
                                        isAtivo: novoCliente.isAtivo,
                                        isAMS: novoCliente.isAMS
                                    }
                                } );
                            }
                        );
                    } );
            } );
        } );
    } );
} );

// @route   POST api/clientes
// @desc    Cria um cliente. Usada por um Administrador, logo, não temos todos os dados
// @acess   Private
router.post( '/ddd', auth, ( req, res ) => {
    /*if ( req.data.type === 'admin' ) {
        const { nomeCliente, email, telefone, rotinas } = req.body;

        //Validações simples
        if ( !nomeCliente ) {
            return res.status( 400 ).json( { msg: 'Digite o Nome do Cliente!' } );
        }
        if ( !email ) {
            return res.status( 400 ).json( { msg: 'Digite o E-mail do Cliente!' } );
        }
        if ( !telefone ) {
            return res.status( 400 ).json( { msg: 'Digite o Telefone do Cliente!' } );
        }
        if ( !rotinas ) {
            return res.status( 400 ).json( { msg: 'Não foram selecionadas Rotinas, contate um Administrador do Sistema!' } );
        }

        //Verificar cliente já cadastrado
        Cliente.findOne( { email } ).then( cliente => {
            if ( cliente ) return res.status( 400 ).json( { msg: 'Cliente já cadastro!' } );
        } );

        const newCliente = new Cliente( {
            nomeCliente: nomeCliente,
            email: email,
            telefone: telefone,
            senha: 'padrao_vitrine',
            rotinas: rotinas,
            isAtivo: true
        } );

        newCliente.save().then( cliente => res.json( cliente ) );

    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }*/
} );

// @route   DELETE api/clientes/:id
// @desc    Deleta um cliente
// @acess   Private
router.delete( '/:id', auth, ( req, res ) => {
    if ( req.data.type === 'admin' ) {
        if ( !req.params.id ) return res.status( 400 ).json( { msg: 'Passe um id!' } );

        Cliente.findById( req.params.id )
            .then( cliente => cliente.remove().then( () => res.json( { sucess: true } ) ) )
            .catch( err => res.status( 404 ).json( { sucess: false } ) );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   PUT api/clientes/:id
// @desc    Atualiza um cliente
// @acess   Private
router.put( '/', auth, ( req, res ) => {
    if ( req.data.type === 'admin' ) {
        if ( !req.body.nomeCliente ) return res.status( 400 ).json( { msg: 'Digite o Nome do Cliente!' } );
        if ( !req.body.codigoClientePC ) return res.status( 400 ).json( { msg: 'Digite o Código do Cliente!' } );
        if ( !req.body.telefone ) return res.status( 400 ).json( { msg: 'Digite o Telefone do Cliente!' } );
        if ( !req.body.rotinas ) { return res.status( 400 ).json( { msg: 'Não foram selecionadas Rotinas, contate um Administrador do Sistema!' } ); }
        Cliente.findByIdAndUpdate(
            req.body.id,
            {
                nomeCliente: req.body.nomeCliente,
                codigoClientePC: req.body.codigoClientePC,
                telefone: req.body.telefone,
                isAtivo: req.body.isAtivo,
                isAMS: req.body.isAMS,
                rotinas: req.body.rotinas
            },
            { new: true },
            function ( err, response ) {
                // Handle any possible database errors
                if ( err ) {
                    res.status( 404 ).json( { sucess: false } );
                }
                res.json( { response } );
            } );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

module.exports = router;