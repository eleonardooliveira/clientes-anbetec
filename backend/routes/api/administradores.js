const express = require( 'express' );
const router = express.Router();
const bcrypt = require( 'bcryptjs' );
const config = require( 'config' );
const jwt = require( 'jsonwebtoken' );
const auth = require( '../../middleware/auth' );

//Administrador Model
const Administrador = require( '../../models/Administrador' );

// @route   GET api/administradores/verifyToken
// @desc    Valida se o token enviado é válido e se não está expirado
// @acess   Public
router.get( '/verifyToken', auth, ( req, res ) => {
    if ( req.data.type === 'admin' ) {
        const token = req.header( 'x-auth-token' );

        //Validando se passou token
        if ( !token ) return res.status( 401 ).json( { msg: 'Token não enviado, acesso negado!' } );

        jwt.verify( token, config.get( 'jwtSecret' ), ( err, verifiedJwt ) => {
            if ( err ) {
                return res.status( 401 ).json( { msg: err.message } );
            } else {
                res.send( verifiedJwt );
            }
        } );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   POST api/administradores
// @desc    Registrar novo administrador
// @acess   Public
router.post( '/', auth, ( req, res ) => {
    if ( req.data.type === 'admin' ) {
        const { nomeAdministrador, email, senha } = req.body;

        //Validações simples
        if ( !nomeAdministrador ) {
            return res.status( 400 ).json( { msg: 'Digite o Nome do Usuário!' } );
        }
        if ( !email ) {
            return res.status( 400 ).json( { msg: 'Digite o E-mail do Usuário!' } );
        }
        if ( !senha ) {
            return res.status( 400 ).json( { msg: 'Digite uma Senha!' } );
        }

        //Verificar usuário já cadastrado
        Administrador.findOne( { email } ).then( administrador => {
            if ( administrador ) return res.status( 400 ).json( { msg: 'Usuário já cadastro!' } );

            const novoAdministrador = new Administrador( {
                nomeAdministrador,
                email,
                senha,
                isAtivo: false
            } );

            //Criando salt & hash
            bcrypt.genSalt( 10, ( err, salt ) => {
                bcrypt.hash( novoAdministrador.senha, salt, ( err, hash ) => {
                    if ( err ) throw err;
                    novoAdministrador.senha = hash;
                    novoAdministrador.save()
                        .then( novoAdministrador => {

                            jwt.sign(
                                { id: novoAdministrador.id },
                                config.get( 'jwtSecret' ),
                                { expiresIn: 3600 },
                                ( err, token ) => {
                                    if ( err ) throw err;

                                    res.json( {
                                        status: "OK",
                                        administrador: {
                                            id: novoAdministrador.id,
                                            nomeAdministrador: novoAdministrador.nomeAdministrador,
                                            email: novoAdministrador.email,
                                            isAtivo: false
                                        }
                                    } );
                                }
                            );
                        } );
                } );
            } );
        } );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   GET api/administradores
// @desc    Retorna todos os administradores ou apenas um se passar ID
// @acess   Private
router.get( '/:idAdministrador?', auth, ( req, res ) => {
    if ( req.data.type === 'admin' ) {
        if ( req.query.idAdministrador ) {//passou id de um cliente específico
            Administrador.findById( req.query.idAdministrador )
                .then( administrador => res.json( administrador ) );
        } else {//não passou id de um cliente específico
            Administrador.find()
                .sort( { date: -1 } )
                .then( administradores => res.json( administradores ) );
        }
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   DELETE api/administradores/:id
// @desc    Deleta um administrador
// @acess   Private
router.delete( '/:id', auth, ( req, res ) => {
    if ( req.data.type === 'admin' ) {
        Administrador.findById( req.params.id )
            .then( administrador => administrador.remove().then( () => res.json( { sucess: true } ) ) )
            .catch( err => res.status( 404 ).json( { sucess: false } ) );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

// @route   PUT api/administradores/:id
// @desc    Atualiza um usuário
// @acess   Private
router.put( '/', auth, ( req, res ) => {
    if ( req.data.type === 'admin' ) {
        const { nomeAdministrador } = req.body;

        //Validações simples
        if ( !nomeAdministrador ) {
            return res.status( 400 ).json( { msg: 'Digite o Nome do Usuário!' } );
        }

        Administrador.findByIdAndUpdate(
            req.body.id,
            {
                nomeAdministrador: req.body.nomeAdministrador,
                isAtivo: req.body.isAtivo
            },
            { new: true },
            function ( err, response ) {
                // Handle any possible database errors
                if ( err ) {
                    res.status( 404 ).json( { sucess: false } );
                }
                res.json( { response } );
            } );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

module.exports = router;