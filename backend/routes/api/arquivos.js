const express = require( 'express' );
const router = express.Router();
const auth = require( '../../middleware/auth' );

const uploadDeArquivo = require( '../../config/multer.config' );

const upload = uploadDeArquivo.default.single( 'file' );

//Arquivo Model
const Arquivo = require( '../../models/Arquivo' );
//Rotina Model
const Rotina = require( '../../models/Rotina' );
//ArquivoLixo Model
const ArquivoLixo = require( '../../models/ArquivoLixo' );

const salvarArquivo = async ( req, res ) => {

    const { originalname, encoding, mimetype, filename, path } = req.file;

    const arquivo = new Arquivo( {
        nomeArquivoUnico: filename,
        nomeArquivoOriginal: originalname,
        encoding,
        tipoArquivo: mimetype,
        urlDownload: path,
        tamanho: req.body.size,
        dataUltAlteracao: req.body.lastModified
    } );

    arquivo.save()
        .then( novoArquivo => {
            return res.json( novoArquivo.id );
        } );

    //Adicionando esse anexo como filho da Rotina
    Rotina.findByIdAndUpdate(
        req.body.idRotina,
        {
            $push: {
                'arquivos': arquivo._id
            }
        },
        { new: true },
        function ( err, response ) {
            // Handle any possible database errors
            if ( err ) {
                res.status( 404 ).json( { sucess: false } );
            }
        } );
};

// @route   POST api/arquivos
// @desc    Salva um arquivo 
// @acess   Private
router.post( '/', auth, upload, salvarArquivo, async ( req, res ) => { } );

// @route   GET api/arquivos
// @desc    Retorna informações de um arquivo
// @acess   Public
router.get( '/', ( req, res ) => {
    Arquivo.findById( req.query.idArquivo )
        .then( arquivo => res.json( arquivo ) );
} );

// @route   DELETE api/arquivos/:id
// @desc    Deleta um arquivo
// @acess   Private
router.delete( '/:id', auth, ( req, res ) => {
    if ( req.data.type === 'parc' || req.data.type === 'admin' ) {
        if ( !req.params.id ) return res.status( 400 ).json( { msg: 'Passe um id!' } );

        //Apagando o arquivo
        Arquivo.findById( req.params.id )
            .then( arquivo => {


                //Não iremos apagar os arquivos do Google Cloud Storage, iremos salvar em uma 'lixeira'
                const arquivoLixo = new ArquivoLixo( {
                    nomeArquivoUnico: arquivo.nomeArquivoUnico,
                    nomeArquivoOriginal: arquivo.nomeArquivoOriginal,
                    encoding: arquivo.encoding,
                    tipoArquivo: arquivo.tipoArquivo,
                    urlDownload: arquivo.urlDownload,
                    tamanho: arquivo.tamanho,
                    dataUltAlteracao: arquivo.dataUltAlteracao,
                    dataUpload: arquivo.dataUpload
                } );

                arquivoLixo.save();

                //Apagando o arquivo
                arquivo.remove().then( () => {
                    //Apagando a referencia do arquivo na Rotina
                    Rotina.find( { arquivos: req.params.id } )
                        .then( rotina => {

                            var array = [...rotina[0].arquivos];
                            var index = array.indexOf( req.params.id );
                            if ( index !== -1 ) {
                                array.splice( index, 1 );
                            }

                            Rotina.findByIdAndUpdate(
                                rotina[0]._id,
                                {
                                    arquivos: array
                                },
                                { new: true },
                                function ( err, response ) {
                                    // Handle any possible database errors
                                    if ( err ) {
                                        res.status( 404 ).json( { sucess: false } );
                                    }

                                    res.json( { sucess: true } );
                                } );
                        } );
                } );
            }
            )
            .catch( err => res.status( 404 ).json( { sucess: false } ) );
    } else {
        return res.status( 401 ).json( { msg: 'O Token passado não tem permissão para realizar essa ação, acesso negado!' } );
    }
} );

module.exports = router;