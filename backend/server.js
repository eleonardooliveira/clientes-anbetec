const express = require( 'express' );
const mongoose = require( 'mongoose' );
const path = require( 'path' );
const config = require( 'config' );
const cors = require( 'cors' );

const app = express();

//Bodyparse Middleware
app.use( express.json() );

//DB Config
const db = config.get( 'mongoURI' );

//Connect to Mongo
mongoose.connect( db,
    {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    } )
    .then( () => console.log( 'MongoDb Connected...' ) )
    .catch( err => console.log( err ) );

app.use( cors() );

//Use Routes
app.use( '/api/clientes', require( './routes/api/clientes' ) );
app.use( '/api/rotinas', require( './routes/api/rotinas' ) );
app.use( '/api/administradores', require( './routes/api/administradores' ) );
app.use( '/api/parceiros', require( './routes/api/parceiros' ) );
app.use( '/api/arquivos', require( './routes/api/arquivos' ) );
app.use( '/api/auth', require( './routes/api/auth' ) );

//Serve static assets if in production
if ( process.env.NODE_ENV === 'production' ) {
    //Set static folder
    app.use( express.static( 'client/build' ) );

    app.get( '*', () => {
        res.sendFile( path.resolve( __dirname, 'client', 'build', 'index.html' ) );
    } );
}

const port = process.env.PORT || 7000;

app.listen( port, "0.0.0.0", () => console.log( `Server started on port ${ port }` ) );