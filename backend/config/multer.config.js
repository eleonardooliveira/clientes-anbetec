const path = require( 'path' );
const multer = require( 'multer' );
const MulterGoogleCloudStorage = require( 'multer-google-storage' );

const optionsGoogleStorage = {
    maxRetries: 2,
    autoRetry: true,
    email: 'eleonardo.oliveira@anbetec.com',
    bucket: 'rotinas_shop',
    projectId: '949445038398',
    keyFilename: path.resolve( __dirname, '..', 'storage.json' )
};

const uploadDeArquivos = multer( {
    storage: MulterGoogleCloudStorage.storageEngine( optionsGoogleStorage )
} );

exports.default = uploadDeArquivos;