const jwt = require( 'jsonwebtoken' );
const config = require( 'config' );

function auth( req, res, next ) {
    const token = req.header( 'x-auth-token' );

    //Validando se passou token
    if ( !token ) return res.status( 401 ).json( { msg: 'Token não enviado, acesso negado!' } );

    try {
        //Verificando o token enviado
        req.data = jwt.verify( token, config.get( 'jwtSecret' ) );

        next();

    } catch ( e ) {
        return res.status( 401 ).json( { msg: 'Token inválido!' } );
    }
}

module.exports = auth;