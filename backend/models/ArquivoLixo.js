const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

//create Schema
const ArquivoLixoSchema = new Schema( {
    nomeArquivoUnico: {
        type: String,
        required: true
    },
    nomeArquivoOriginal: {
        type: String,
        required: true
    },
    encoding: {
        type: String,
        required: true
    },
    tipoArquivo: {
        type: String,
        required: true
    },
    urlDownload: {
        type: String,
        required: true
    },
    tamanho: {
        type: String,
        required: true
    },
    dataUltAlteracao: {
        type: Date,
        required: true,
        default: Date.now
    },
    dataUpload: {
        type: Date,
        required: true,
        default: Date.now
    },
    dataExclusao: {
        type: Date,
        required: true,
        default: Date.now
    }
} );

module.exports = ArquivoLixo = mongoose.model( 'arquivoLixo', ArquivoLixoSchema );