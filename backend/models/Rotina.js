const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

//create Schema
const RotinaSchema = new Schema( {
    nomeRotina: {
        type: String,
        required: true
    },
    descricaoRotina: {
        type: String,
        required: true
    },
    descricaoCompletaRotina: {
        type: String,
        required: true
    },
    modulo: {
        type: String,
        required: true
    },
    vendaMensal: {
        type: Boolean,
        required: true
    },
    valorMensal: {
        type: String,
        required: true
    },
    vendaVitalicio: {
        type: Boolean,
        required: true
    },
    valorVitalicio: {
        type: String,
        required: true
    },
    idParceiroDono: {
        type: String,
        required: true
    },
    arquivos: {
        type: [String],
        default: []
    },
    isAtivo: {
        type: Boolean,
        default: true
    },
    countClicks: {
        type: Number,
        default: 0
    },
    countInterests: {
        type: Number,
        default: 0
    },
    data: {
        type: Date,
        default: Date.now
    }
} );

RotinaSchema.index( {
    nomeRotina: 'text',
    descricaoRotina: 'text',
    descricaoCompletaRotina: 'text',
} );

module.exports = Rotina = mongoose.model( 'rotina', RotinaSchema );