const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

const RotinaSchema = require( './Rotina' );

//create Schema
const ClienteSchema = new Schema( {
    nomeCliente: {
        type: String,
        required: true
    },
    codigoClientePC: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    telefone: {
        type: String,
        required: true
    },
    senha: {
        type: String,
        required: true
    },
    rotinas: {
        type: Array,
        default: []
    },
    isAtivo: {
        type: Boolean,
        default: true
    },
    isAMS: {
        type: Boolean,
        default: false
    },
    data: {
        type: Date,
        default: Date.now
    }
} );

module.exports = Cliente = mongoose.model( 'cliente', ClienteSchema );