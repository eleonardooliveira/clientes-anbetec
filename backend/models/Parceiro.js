const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

//create Schema
const ParceiroSchema = new Schema( {
    nomeParceiro: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    telefone: {
        type: String,
        required: true
    },
    senha: {
        type: String,
        required: true
    },
    isAtivo: {
        type: Boolean,
        default: false
    },
    rotinas: {
        type: Array,
        default: []
    },
    data_registro: {
        type: Date,
        default: Date.now
    },
    dataRecuperacaoSenha: {
        type: Date,
        required: false,
        default: ''
    },
    codigoRecuperacaoSenha: {
        type: String,
        required: false,
        default: ''
    }
} );

ParceiroSchema.methods.toJSON = function () {
    var obj = this.toObject();
    delete obj.senha;
    return obj;
};

module.exports = Parceiro = mongoose.model( 'parceiro', ParceiroSchema );