const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

//create Schema
const AdministradorSchema = new Schema( {
    nomeAdministrador: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    senha: {
        type: String,
        required: true
    },
    isAtivo: {
        type: Boolean,
        default: false
    },
    data_registro: {
        type: Date,
        default: Date.now
    }
} );

AdministradorSchema.methods.toJSON = function () {
    var obj = this.toObject();
    delete obj.senha;
    return obj;
};

module.exports = Administrador = mongoose.model( 'Administradore', AdministradorSchema );