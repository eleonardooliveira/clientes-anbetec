import React from "react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
import SweetAlert from "react-bootstrap-sweetalert";

// @material-ui/icons
import Email from "@material-ui/icons/Email";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";

import styles from "assets/jss/material-dashboard-pro-react/views/loginPageStyle.js";
import Api from "services/Api";
import { verifyEmail } from "services/validations";
import { login } from "services/AuthService";

const useStyles = makeStyles( styles );

export default function LoginPage( props ) {
  const [cardAnimaton, setCardAnimation] = React.useState( "cardHidden" );
  setTimeout( function () { setCardAnimation( "" ); }, 700 );

  const [email, setEmail] = React.useState( "" );
  const [loginEmailState, setLoginEmailState] = React.useState( "" );
  const [senha, setSenha] = React.useState( "" );
  const [isDeslogado, setIsDeslogado] = React.useState( false );
  const [alert, setAlert] = React.useState( null );

  React.useLayoutEffect( () => {
    const fetchData = async () => {

      const { deslogado } = props.match.params;

      deslogado ? setIsDeslogado( deslogado ) : setIsDeslogado( false );

    };
    fetchData();
  }, [] );

  const classes = useStyles();

  const handleSignIn = async e => {

    if ( email && senha && loginEmailState === "success" ) {
      try {
        const res = await Api.post( '/auth/parceiro', JSON.stringify( { email, senha } ) );

        login( res.data.token );

        props.history.push( "/admin/rotinas" );
      } catch ( err ) {
        openAlert( { message: err.response.data.msg } );
      }
    } else {
      if ( !email ) {
        openAlert( { message: 'Insira o E-mail!' } );
      } else if ( loginEmailState === "error" ) {
        openAlert( { message: 'O E-mail inserido é inválido!' } );
      } else if ( !senha ) {
        openAlert( { message: 'Insira a senha!' } );
      }
    }
  };

  const openAlert = ( { message } ) => {
    setAlert(
      <SweetAlert
        danger
        style={{ display: "block", marginTop: "-100px" }}
        showCancel={false}
        title={message}
        confirmBtnText={'Certo!'}
        onConfirm={async () => {
          setAlert( null );
        }}
        confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>

      </SweetAlert > );

  };

  const inputEmailAlert = () => {
    setAlert(
      <SweetAlert
        input
        showCancel
        cancelBtnText={'Cancelar'}
        confirmBtnText={'Pronto'}
        validationMsg={'Insira um e-mail válido!'}
        validationRegex={/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/}
        style={{ display: "block", marginTop: "-100px" }}
        title="Digite o e-mail cadastrado:"
        onConfirm={async email => {
          await Api.put( '/auth/recuperarSenha', JSON.stringify( { email } ) );
          hideAlert();
          inputCodigoAlert( email );
        }}
        onCancel={() => hideAlert()}
        confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}
        cancelBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}
      />
    );
  };

  const inputCodigoAlert = ( email ) => {
    setAlert(
      <SweetAlert
        input
        showCancel={false}
        confirmBtnText={'Enviar'}
        validationMsg={'Formato do código inválido!'}
        defaultValue={''}
        validationRegex={/\(|\)|\d{5}/}
        style={{ display: "block", marginTop: "-100px" }}
        title="Digite o código recebido:"
        onConfirm={async codigo => {
          try {
            const response = await Api.put( '/auth/validarCodigoRecuperarSenha', JSON.stringify( { email, codigo } ) );
            console.log( response );

            hideAlert();
            inputNovaSenhaAlert( email );

          } catch ( err ) {
            inputCodigoErradoAlert( err.response.data.msg, email );
          }
        }}
        confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}
        cancelBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}
      >Se o e-mail inserido estiver cadastrado, você já recebeu um código no mesmo, insira-o abaixo:</SweetAlert>
    );
  };

  const inputCodigoErradoAlert = ( mensagem, email ) => {
    setAlert(
      <SweetAlert
        danger
        showCancel={false}
        confirmBtnText={'Certo!'}
        style={{ display: "block", marginTop: "-100px" }}
        title={mensagem}
        onConfirm={() => {
          inputCodigoAlert( email );
        }}
        confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}
        cancelBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}
      />
    );
  };

  const inputNovaSenhaAlert = ( email ) => {
    setAlert(
      <SweetAlert
        input
        inputType={'password'}
        showCancel={false}
        confirmBtnText={'Pronto'}
        validationMsg={'Senha inválida, digite pelo menos 6 caracteres!'}
        validationRegex={/^.{6,}$/}
        style={{ display: "block", marginTop: "-100px" }}
        title="Digite a nova senha:"
        onConfirm={async senha => {
          const response = await Api.put( '/auth/alterarSenha', JSON.stringify( { email, senha } ) );

          if ( response.data.sucess === true ) {
            setTimeout( () => { hideAlert(); }, 3000 );
            hideAlert();
            inputNovaSenhaSucessoAlert();
          }
        }}
        onCancel={() => hideAlert()}
        confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}
        cancelBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}
      />
    );
  };

  const inputNovaSenhaSucessoAlert = ( email ) => {
    setAlert(
      <SweetAlert
        success
        style={{ display: "block", marginTop: "-100px" }}
        showCancel={false}
        confirmBtnText={''}
        title={''}
        confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
        Senha alterada com sucesso, você pode realizar login com a nova senha agora!
      </SweetAlert>
    );
  };

  const hideAlert = () => {
    setAlert( null );
  };

  return (
    <div className={classes.container}>
      {/* CX. DIALOGO */}
      <div>{alert}</div>
      <GridContainer justify="center">
        <GridItem xs={12} sm={6} md={4}>
          <form>
            <Card login className={classes[cardAnimaton]}>
              <CardHeader
                className={`${ classes.cardHeader } ${ classes.textCenter }`}
                color="rose"
              >
                <h4 className={classes.cardTitle}>Log in</h4>
              </CardHeader>
              <CardBody>
                {isDeslogado ? <h4 align="center" id="textoDeslogado">Sua sessão expirou, por favor, logue novamente</h4> : null}
                <CustomInput
                  labelText="Email..."
                  id="email"
                  success={loginEmailState === "success"}
                  error={loginEmailState === "error"}
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <Email className={classes.inputAdornmentIcon} />
                      </InputAdornment>
                    ),
                    onChange: event => {
                      if ( verifyEmail( event.target.value ) ) {
                        setLoginEmailState( "success" );
                      } else {
                        setLoginEmailState( "error" );
                      }
                      setEmail( event.target.value );
                    },
                    type: "email"
                  }}
                />
                <CustomInput
                  labelText="Senha"
                  id="senha"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <Icon className={classes.inputAdornmentIcon}>
                          lock_outline
                        </Icon>
                      </InputAdornment>
                    ), onChange: event => { setSenha( event.target.value ); },
                    type: "password",
                    autoComplete: "off"
                  }}
                />
                <div className={classes.center}>
                  <Button
                    color="rose"
                    onClick={inputEmailAlert}
                    simple size="sm">
                    Esqueci minha senha
                </Button>
                </div>
              </CardBody>
              <CardFooter className={classes.justifyContentCenter}>
                <Button
                  color="rose"
                  simple size="lg"
                  block
                  onClick={handleSignIn}>
                  Let{"'"}s Go
                </Button>
              </CardFooter>
            </Card>
          </form>
        </GridItem>
      </GridContainer>
    </div>
  );
};
