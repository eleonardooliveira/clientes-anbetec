import React from "react";

import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import FormLabel from "@material-ui/core/FormLabel";
import SweetAlert from "react-bootstrap-sweetalert";

import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";

import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardText from "components/Card/CardText.js";
import CardBody from "components/Card/CardBody.js";

import Api from '../../../services/Api';

import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import styles from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.js";

import { verifyEmail } from "services/validations";
import { formatPhone } from "services/utils";

class PartnersFormComponent extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            id: '',
            name: '',
            email: '',
            telefone: '',
            inputNameState: '',
            senhaAntiga: '',
            senhaNova: '',
            alert: null
        };
    }

    async componentDidMount() {

        const result = await Api.get( '/parceiros/infosParceiroLogado' );

        this.setState( {
            id: result.data._id,
            name: result.data.nomeParceiro,
            email: result.data.email,
            telefone: formatPhone( result.data.telefone )
        } );

    }

    setAlert( arg ) {
        this.setState( { alert: arg } );
    }

    onChange = ( e ) => {
        this.setState( { [e.target.name]: e.target.value } );
    };

    setInputNameState = ( valueStatus ) => {
        this.setState( {
            inputNameState: valueStatus
        } );

    };

    setInputTelefoneState = ( valueStatus ) => {
        this.setState( {
            inputTelefoneState: valueStatus
        } );
    };

    setInputEmailState = ( valueStatus ) => {
        this.setState( {
            inputEmailState: valueStatus
        } );
    };

    salvar = async () => {

        try {

            if ( this.state.name.trim().length < 6 ) {
                this.openAlert( { redirect: false, message: 'Nome inválido, digite pelo menos 6 letras!' } );
                this.setInputNameState( "error" );
                return;
            }

            if ( !this.state.telefone ) {
                this.openAlert( { redirect: false, message: 'Insira o Telefone!' } );
                this.setInputTelefoneState( "error" );
                return;
            }

            if ( this.state.telefone.trim().length < 14 ) {
                this.openAlert( { redirect: false, message: 'Telefone inválido!' } );
                this.setInputTelefoneState( "error" );
                return;
            }

            if ( !this.state.email ) {
                this.openAlert( { redirect: false, message: 'Insira o E-mail!' } );
                this.setInputEmailState( "error" );
                return;
            }

            if ( this.state.inputEmailState === "error" ) {
                this.openAlert( { redirect: false, message: 'E-mail inválido!' } );
                return;
            }

            if ( ( this.state.senhaAntiga.trim() !== "" ) !== ( this.state.senhaNova.trim() !== "" ) ) {//se tiver preenchido apenas um dos campos de senha
                this.openAlert( { redirect: false, message: 'Para alterar a senha é preciso digitar a Senha Atual e a Senha Nova!' } );
                return;
            }

            if ( ( this.state.senhaAntiga.trim().length < 6 ) ) {
                this.openAlert( { redirect: false, message: 'Senha Antiga inválida, digite pelo menos 6 caracteres!' } );
                return;
            }

            if ( ( this.state.senhaNova.trim().length < 6 ) ) {
                this.openAlert( { redirect: false, message: 'Senha Nova inválida, digite pelo menos 6 caracteres!' } );
                return;
            }

            var changingPassword = false;
            if ( ( this.state.senhaAntiga.trim() !== "" ) === true && ( this.state.senhaNova.trim() !== "" ) === true )
                changingPassword = true;

            this.setInputNameState( "success" );
            this.setInputTelefoneState( "success" );
            this.setInputEmailState( "success" );

            await Api.put( `/parceiros/atualizarProprio`,
                {
                    id: this.state.id,
                    nomeParceiro: this.state.name,
                    telefone: this.state.telefone.replace( /\D/g, '' ),
                    changingPassword: changingPassword,
                    senhaAntiga: this.state.senhaAntiga.trim(),
                    senhaNova: this.state.senhaNova.trim()
                } );

            setTimeout( () => { this.props.history.push( '/admin/rotinas' ); }, 2000 );
            this.openAlert( { redirect: true, message: 'Cadastro salvo com sucesso!' } );

        } catch ( err ) {
            if ( err.response )
                this.openAlert( { redirect: false, message: err.response.data.msg } );
            else
                this.openAlert( { redirect: false, message: err } );
        }
    };

    toggleCheckbox( event ) {
        switch ( event.target.value ) {
            case "checkAtivo":
                let newValue = ( this.state.checkAtivo === "on" || this.state.checkAtivo === true ) ? false : true;

                if ( newValue === false ) {//se está desativando, deve impossibilitar de acessar rotinas tm
                    this.setState( {
                        checkAtivo: false
                    } );
                } else {
                    this.setState( {
                        checkAtivo: true
                    } );
                }
                break;

            default:
                break;
        }
    }

    hideAlert = () => {
        this.setAlert( null );
    };

    openAlert = ( { redirect, message } ) => {
        if ( redirect ) {
            this.setAlert(
                <SweetAlert
                    success
                    style={{ display: "block", marginTop: "-200px" }}
                    showCancel={false}
                    title={message}
                    confirmBtnText={''}
                    onConfirm={async () => {
                        this.setAlert( null );
                    }}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>

                </SweetAlert> );
        } else {
            this.setAlert(
                <SweetAlert
                    danger
                    style={{ display: "block", marginTop: "-200px" }}
                    showCancel={false}
                    title={message}
                    confirmBtnText={'Certo!'}
                    onConfirm={async () => {
                        this.setAlert( null );
                    }}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>

                </SweetAlert> );
        }
    };

    render() {

        const classes = makeStyles( styles );

        return (
            <GridContainer>
                <GridItem md={12}>
                    {/* CX. DIALOGO */}
                    <div>{this.state.alert}</div>
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose" text>
                            <CardText color="rose">
                                <h4 className={classes.cardTitle}>Edição de Cadastro</h4>
                            </CardText>
                        </CardHeader>
                        <CardBody>
                            <form>
                                <GridContainer>
                                    <GridItem xs={12} sm={10} md={6}>
                                        <legend>Dados</legend>
                                        <CustomInput
                                            id="name"
                                            name="name"
                                            labelText={<span>Nome: <small>(obrigatório)</small></span>}
                                            success={this.state.inputNameState === "success"}
                                            error={this.state.inputNameState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                placeholder: "Nome",
                                                value: this.state.name,
                                                onChange: event => {
                                                    if ( event.target.value.trim().length >= 6 ) {
                                                        this.setInputNameState( "success" );
                                                    } else {
                                                        this.setInputNameState( "error" );
                                                    }
                                                    this.setState( {
                                                        name: event.target.value
                                                    } );
                                                },
                                                type: "text"
                                            }}
                                        />
                                        <CustomInput
                                            id="telefone"
                                            name="telefone"
                                            labelText={<span>Telefone: <small>(obrigatório)</small></span>}
                                            success={this.state.inputTelefoneState === "success"}
                                            error={this.state.inputTelefoneState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                value: this.state.telefone,
                                                onChange: event => {
                                                    if ( event.target.value.trim().length >= 14 ) {
                                                        this.setInputTelefoneState( "success" );
                                                    } else {
                                                        this.setInputTelefoneState( "error" );
                                                    }
                                                    this.setState( {
                                                        telefone: formatPhone( event.target.value )
                                                    } );
                                                },
                                                type: "text"
                                            }}
                                        />
                                        <CustomInput
                                            id="email"
                                            name="email"
                                            labelText={<span>E-mail: <small>(obrigatório)</small></span>}
                                            success={this.state.inputEmailState === "success"}
                                            error={this.state.inputEmailState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                value: this.state.email,
                                                onChange: event => {
                                                    if ( verifyEmail( event.target.value ) ) {
                                                        this.setInputEmailState( "success" );
                                                    } else {
                                                        this.setInputEmailState( "error" );
                                                    }
                                                    this.setState( {
                                                        email: event.target.value
                                                    } );
                                                },
                                                type: "email",
                                                disabled: true
                                            }}
                                        />
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <legend>Alterar Senha</legend>
                                        <div className={classes.block}>
                                            <FormLabel className={classes.labelHorizontal}>Senha Atual</FormLabel>
                                            <CustomInput
                                                id="senhaAntiga"
                                                name="senhaAntiga"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    endAdornment: (
                                                        <InputAdornment position="end">
                                                            <Icon className={classes.inputAdornmentIcon}>
                                                                lock_outline
                                        </Icon>
                                                        </InputAdornment>
                                                    ), onChange: event => {
                                                        this.setState( {
                                                            senhaAntiga: event.target.value
                                                        } );

                                                    },
                                                    type: "password",
                                                    autoComplete: "off"
                                                }}
                                            />
                                            <FormLabel className={classes.labelHorizontal}>Nova Senha</FormLabel>
                                            <CustomInput
                                                id="senhaNova"
                                                name="senhaNova"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    endAdornment: (
                                                        <InputAdornment position="end">
                                                            <Icon className={classes.inputAdornmentIcon}>
                                                                lock_outline
                                        </Icon>
                                                        </InputAdornment>
                                                    ), onChange: event => {
                                                        this.setState( {
                                                            senhaNova: event.target.value
                                                        } );

                                                    },
                                                    type: "password",
                                                    autoComplete: "off"
                                                }}
                                            />
                                        </div>
                                    </GridItem>
                                </GridContainer>
                                <br />
                                <br />
                                <Button
                                    color="rose"
                                    onClick={this.salvar}
                                    className={classes.registerButton}>
                                    Salvar
                                </Button>
                            </form>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer >
        );
    }
}

PartnersFormComponent.propTypes = { classes: PropTypes.object };
export default withStyles( {
    infoText: { fontWeight: "300", margin: "10px 0 30px", textAlign: "center" },
    inputAdornmentIcon: { color: "#555" },
    inputAdornment: { position: "relative" }
} )( PartnersFormComponent );;