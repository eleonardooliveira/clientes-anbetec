import React from 'react';

import ReactTable from "react-table";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import SweetAlert from "react-bootstrap-sweetalert";

// @material-ui/icons
import Assignment from "@material-ui/icons/Assignment";
import Close from "@material-ui/icons/Close";

import Api from '../../../services/Api';

import { Link } from 'react-router-dom';

// core components
import GridContainer from "components/Grid/GridContainer.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import GridItem from "components/Grid/GridItem.js";
import CardBody from "components/Card/CardBody.js";
import CardIcon from "components/Card/CardIcon.js";
import CardHeader from "components/Card/CardHeader.js";


import { cardTitle } from "assets/jss/material-dashboard-pro-react.js";

import { formatDate } from 'services/utils';

const styles = {
    cardIconTitle: {
        ...cardTitle,
        marginTop: "15px",
        marginBottom: "0px"
    }
};

const useStyles = makeStyles( styles );

export default function PartnersReactTable( props ) {
    const [parceiros, setParceiros] = React.useState( props.parceiros );
    const [alert, setAlert] = React.useState( null );
    React.useEffect( () => setParceiros( props.parceiros ), [props.parceiros] );

    const classes = useStyles();

    const openAlert = ( { type, parceiro } ) => {
        if ( type === 'delete' )
            setAlert(
                <SweetAlert
                    danger
                    style={{ display: "block", marginTop: "-200px" }}
                    showCancel={true}
                    title="Confirmação"
                    confirmBtnText={'Confirmar'}
                    cancelBtnText={'Cancelar'}
                    onConfirm={async () => {
                        var newParceiros = parceiros;

                        for ( var i = 0; i < parceiros.length; i++ ) {
                            if ( parceiros[i]._id === parceiro._id ) {
                                //excluindo do banco
                                const res = await Api.delete( `/parceiros/${ parceiro._id }` );

                                if ( res.status === 200 ) {
                                    //excluindo da tela
                                    newParceiros.splice( i, 1 );
                                    setParceiros( [...newParceiros] );
                                }
                                break;
                            }

                        }
                        setAlert( null );
                    }}
                    onCancel={() => {
                        setAlert( null );
                    }}
                    cancelBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 MuiButton-text`}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
                    Deseja realmente excluir o Parceiro '<b>{parceiro.nomeParceiro}</b>'' ?
          </SweetAlert > );

    };

    return (
        <GridContainer>
            <GridItem md={12}>
                {/* CX. DIALOGO */}
                <div>{alert}</div>
            </GridItem>
            <GridItem xs={12}>
                <div style={{ display: "flex" }}>
                    <Button
                        xs={3}
                        style={{ marginLeft: "auto" }}
                        color="info"
                        onClick={() => {
                            props.history.push( '/admin/parceiro' );
                        }}>
                        Cadastrar Parceiro
                     </Button>
                </div>
                <Card>
                    <CardHeader color="primary" icon>
                        <CardIcon color="primary">
                            <Assignment />
                        </CardIcon>
                        <h4 className={classes.cardIconTitle}>Parceiros</h4>
                    </CardHeader>
                    <CardBody>
                        <ReactTable
                            data={
                                parceiros.map( parceiro => {
                                    return ( {
                                        id: parceiro._id,
                                        nome: parceiro.nomeParceiro,
                                        email: parceiro.email,
                                        data_registro: ( formatDate( parceiro.data_registro ) ),
                                        isAtivo: parceiro.isAtivo === true ? "Sim" : "Não",
                                        actions: (
                                            <div className="actions-right">
                                                <Link to={`/admin/parceiro/${ parceiro._id }`} className="btn btn-primary" > Editar</Link>
                                                <Button
                                                    justIcon
                                                    round
                                                    simple
                                                    onClick={() => {
                                                        openAlert( { type: 'delete', parceiro: parceiro } );
                                                    }}
                                                    color="danger"
                                                    className="remove"
                                                >
                                                    <Close />
                                                </Button>{" "}</div>
                                        )
                                    } );
                                } )
                            }
                            columns={[
                                {
                                    Header: "Nome",
                                    accessor: "nome",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "E-mail",
                                    accessor: "email",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "Data de Cadastro",
                                    accessor: "data_registro",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "Ativo",
                                    accessor: "isAtivo",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "Ações",
                                    accessor: "actions",
                                    sortable: false,
                                    filterable: false,
                                    resizable: false
                                }
                            ]}
                            defaultPageSize={( parceiros.length === 0 ) ? 5 : parceiros.length}
                            minRows={1}
                            showPaginationTop
                            showPaginationBottom={false}
                            nextText={'Próximo'}
                            previousText={'Anterior'}
                            rowsText={'parceiros'}
                            pageText={'Página'}
                            ofText={'de'}
                            loadingText='Carregando...'
                            noDataText='Nenhum Parceiro encontrado'
                            className="-striped -highlight"
                        />

                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
    );
};