import React from "react";

import PartnersReactTable from './PartnersReactTable.js';

import Api from '../../../services/Api';

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import PropTypes from "prop-types";

import withStyles from "@material-ui/core/styles/withStyles";

class PartnersTableComponent extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            parceiros: []
        };
    }

    async componentDidMount() {
        const result = await Api.get( `/parceiros` );
        this.setState( { parceiros: result.data } );
    }

    render() {
        return (
            <React.Fragment>
                <GridContainer justify="center">
                    <GridItem md={12}>

                        <PartnersReactTable
                            parceiros={this.state.parceiros}
                            history={this.props.history} />
                    </GridItem>
                </GridContainer>

            </React.Fragment>
        );
    }
}

PartnersTableComponent.propTypes = { classes: PropTypes.object };
export default withStyles( {
    infoText: { fontWeight: "300", margin: "10px 0 30px", textAlign: "center" },
    inputAdornmentIcon: { color: "#555" },
    inputAdornment: { position: "relative" }
} )( PartnersTableComponent );