import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
import SweetAlert from "react-bootstrap-sweetalert";
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
import PhoneIcon from '@material-ui/icons/Phone';
import BusinessIcon from '@material-ui/icons/Business';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import InfoArea from "components/InfoArea/InfoArea.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import Api from "services/Api";
import { verifyEmail } from "services/validations";
import { formatPhone } from "services/utils";
import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";

const useStyles = makeStyles( styles );

export default function RegisterPage( props ) {
  const classes = useStyles();

  const [nome, setNome] = React.useState( "" );
  const [registerNomeState, setRegisterNomeState] = React.useState( "" );
  const [email, setEmail] = React.useState( "" );
  const [registerEmailState, setRegisterEmailState] = React.useState( "" );
  const [telefone, setTelefone] = React.useState( "" );
  const [registerTelefoneState, setRegisterTelefoneState] = React.useState( "" );
  const [senha, setSenha] = React.useState( "" );
  const [registerSenhaState, setRegisterSenhaState] = React.useState( "" );
  const [alert, setAlert] = React.useState( null );

  const handleRegister = async e => {

    if ( nome && email && senha &&
      registerNomeState === "success" && registerTelefoneState === "success" && registerEmailState === "success" && registerSenhaState === "success" ) {
      try {
        const res = await Api.post( '/parceiros', {
          nomeParceiro: nome,
          email,
          telefone,
          senha
        } );

        if ( res.data.status === "OK" ) {
          setTimeout( () => { props.history.push( '/auth/login' ); }, 2000 );
          openAlert( { redirect: true, message: 'Registro salvo com sucesso! Você pode fazer login agora!' } );
        } else
          openAlert( { redirect: false, message: res.data.status } );
      } catch ( err ) {
        openAlert( { redirect: false, message: err.response.data.msg } );
      }
    } else {
      if ( !nome ) {
        openAlert( { redirect: false, message: 'Insira seu Nome Completo!' } );
      } else if ( registerNomeState === "error" ) {
        openAlert( { redirect: false, message: 'Nome inválido, digite pelo menos 6 letras!' } );
      } else if ( !telefone ) {
        openAlert( { redirect: false, message: 'Insira seu Telefone!' } );
      } else if ( registerTelefoneState === "error" ) {
        openAlert( { redirect: false, message: 'Telefone inválido!' } );
      } else if ( !email ) {
        openAlert( { redirect: false, message: 'Insira seu E-mail!' } );
      } else if ( registerEmailState === "error" ) {
        openAlert( { redirect: false, message: 'E-mail inválido!' } );
      } else if ( !senha ) {
        openAlert( { redirect: false, message: 'Insira sua Senha!' } );
      } else if ( registerSenhaState === "error" ) {
        openAlert( { redirect: false, message: 'Senha inválida, digite pelo menos 6 caracteres!' } );
      }
    }
  };

  const openAlert = ( { redirect, message } ) => {

    if ( redirect ) {
      setAlert(
        <SweetAlert
          success
          style={{ display: "block", marginTop: "-200px" }}
          showCancel={false}
          title={message}
          confirmBtnText={'Certo!'}
          onConfirm={async () => {
            if ( redirect )
              props.history.push( "/auth" );

            setAlert( null );
          }}
          confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>

        </SweetAlert > );
    } else {
      setAlert(
        <SweetAlert
          danger
          style={{ display: "block", marginTop: "-200px" }}
          showCancel={false}
          title={message}
          confirmBtnText={'Certo!'}
          onConfirm={async () => {
            if ( redirect )
              props.history.push( "/auth" );

            setAlert( null );
          }}
          confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>

        </SweetAlert > );
    }

  };

  return (
    <div className={classes.container}>
      {/* CX. DIALOGO */}
      <div>{alert}</div>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={10}>
          <Card className={classes.cardSignup}>
            <h2 className={classes.cardTitle}>Registrar</h2>
            <CardBody>
              <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={5}>
                  <InfoArea
                    title="Visibilidade"
                    description="Vendendo aqui suas Rotinas, elas terão visibilidade entre os grandes players do Winthor como jamais tiveram."
                    icon={BusinessIcon}
                    iconColor="rose"
                  />
                  <InfoArea
                    title="Ajuda nas Vendas"
                    description="Disponibilizando em nossa loja suas Rotinas, suas vendas irão disparar!"
                    icon={MonetizationOnIcon}
                    iconColor="primary"
                  />
                  <InfoArea
                    title="Oportunidade de Parcerias"
                    description="Com suas Rotinas em nossa loja as oportunidades de conseguir mais parcerias só melhoram!"
                    icon={SupervisedUserCircleIcon}
                    iconColor="info"
                  />
                </GridItem>
                <GridItem xs={12} sm={8} md={5}>
                  <form className={classes.form}>
                    <CustomInput
                      success={registerNomeState === "success"}
                      error={registerNomeState === "error"}
                      formControlProps={{
                        fullWidth: true,
                        className: classes.customFormControlClasses
                      }}
                      inputProps={{
                        startAdornment: (
                          <InputAdornment
                            position="start"
                            className={classes.inputAdornment}
                          >
                            <Face className={classes.inputAdornmentIcon} />
                          </InputAdornment>
                        ),
                        onChange: event => {
                          if ( event.target.value.trim().length >= 6 ) {
                            setRegisterNomeState( "success" );
                          } else {
                            setRegisterNomeState( "error" );
                          }
                          setNome( event.target.value );
                        },
                        placeholder: "Nome Completo..."
                      }}
                    />
                    <CustomInput
                      success={registerTelefoneState === "success"}
                      error={registerTelefoneState === "error"}
                      formControlProps={{
                        fullWidth: true,
                        className: classes.customFormControlClasses
                      }}
                      inputProps={{
                        startAdornment: (
                          <InputAdornment
                            position="start"
                            className={classes.inputAdornment}
                          >
                            <PhoneIcon className={classes.inputAdornmentIcon} />
                          </InputAdornment>
                        ), onChange: event => {
                          event.target.value = formatPhone( event.target.value );

                          if ( event.target.value.trim().length >= 14 ) {
                            setRegisterTelefoneState( "success" );
                          } else {
                            setRegisterTelefoneState( "error" );
                          }

                          setTelefone( event.target.value );
                        },
                        placeholder: "Telefone..."
                      }}
                    />
                    <CustomInput
                      success={registerEmailState === "success"}
                      error={registerEmailState === "error"}
                      formControlProps={{
                        fullWidth: true,
                        className: classes.customFormControlClasses
                      }}
                      inputProps={{
                        startAdornment: (
                          <InputAdornment
                            position="start"
                            className={classes.inputAdornment}
                          >
                            <Email className={classes.inputAdornmentIcon} />
                          </InputAdornment>
                        ), onChange: event => {
                          if ( verifyEmail( event.target.value ) ) {
                            setRegisterEmailState( "success" );
                          } else {
                            setRegisterEmailState( "error" );
                          }
                          setEmail( event.target.value );
                        },
                        placeholder: "E-mail..."
                      }}
                    />
                    <CustomInput
                      success={registerSenhaState === "success"}
                      error={registerSenhaState === "error"}
                      formControlProps={{
                        fullWidth: true,
                        className: classes.customFormControlClasses
                      }}
                      inputProps={{
                        startAdornment: (
                          <InputAdornment
                            position="start"
                            className={classes.inputAdornment}
                          >
                            <Icon className={classes.inputAdornmentIcon}>
                              lock_outline
                            </Icon>
                          </InputAdornment>
                        ), onChange: event => {
                          if ( event.target.value.trim().length >= 6 ) {
                            setRegisterSenhaState( "success" );
                          } else {
                            setRegisterSenhaState( "error" );
                          }
                          setSenha( event.target.value );
                        },
                        type: "password",
                        placeholder: "Senha..."
                      }}
                    />

                    <div className={classes.center}>
                      <Button
                        round
                        color="primary"
                        block
                        onClick={handleRegister}>
                        Let{"'"}s Go
                      </Button>
                    </div>
                  </form>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
