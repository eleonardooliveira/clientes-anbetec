import React from 'react';

import ReactTable from "react-table";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Assignment from "@material-ui/icons/Assignment";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import GridItem from "components/Grid/GridItem.js";
import CardBody from "components/Card/CardBody.js";
import CardIcon from "components/Card/CardIcon.js";
import CardHeader from "components/Card/CardHeader.js";

import { Link } from 'react-router-dom';

import { cardTitle } from "assets/jss/material-dashboard-pro-react.js";

const styles = {
    cardIconTitle: {
        ...cardTitle,
        marginTop: "15px",
        marginBottom: "0px"
    }
};

const useStyles = makeStyles( styles );

export default function RoutinesReactTable( props ) {
    const [rotinas, setRotinas] = React.useState( props.rotinas );
    React.useEffect( () => setRotinas( props.rotinas ), [props.rotinas] );

    const classes = useStyles();

    return (
        <GridContainer>
            <GridItem xs={12}>
                <div style={{ display: "flex" }}>
                    <Button
                        xs={3}
                        style={{ marginLeft: "auto" }}
                        color="info"
                        onClick={() => {
                            props.history.push( '/admin/rotina' );
                        }}>
                        Cadastrar Rotina
                     </Button>
                </div>
                <Card>
                    <CardHeader color="primary" icon>
                        <CardIcon color="primary">
                            <Assignment />
                        </CardIcon>
                        <h4 className={classes.cardIconTitle}>Rotinas</h4>
                    </CardHeader>
                    <CardBody>
                        <ReactTable
                            data={
                                rotinas.map( rotina => {
                                    return ( {
                                        id: rotina._id,
                                        nome: rotina.nomeRotina,
                                        ativo: rotina.isAtivo === true ? "Sim" : "Não",
                                        actions: (
                                            <div className="actions-right">
                                                <Link to={`/admin/rotina/${ rotina._id }`} className="btn btn-pink" >Editar</Link>
                                            </div>
                                        )
                                    } );
                                } )
                            }
                            columns={[
                                {
                                    Header: "Nome",
                                    accessor: "nome",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "Ativa",
                                    accessor: "ativo",
                                    sortable: true,
                                    filterable: true,
                                    resizable: false
                                },
                                {
                                    Header: "Ações",
                                    accessor: "actions",
                                    sortable: false,
                                    filterable: false,
                                    resizable: false
                                }
                            ]}
                            defaultPageSize={( rotinas.length === 0 ) ? 5 : rotinas.length}
                            minRows={1}
                            showPaginationTop
                            showPaginationBottom={false}
                            nextText={'Próximo'}
                            previousText={'Anterior'}
                            rowsText={'rotinas'}
                            pageText={'Página'}
                            ofText={'de'}
                            loadingText='Carregando...'
                            noDataText='Nenhuma Rotina encontrada'
                            className="-striped -highlight"
                        />

                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer >
    );
};