import React from "react";

import Api from '../../../services/Api';

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import PropTypes from "prop-types";

import withStyles from "@material-ui/core/styles/withStyles";
import RoutinesReactTable from "./RoutinesReactTable";

class RoutinesTableComponent extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            rotinas: []
        };
    }

    async componentDidMount() {
        const result = await Api.get( '/parceiros/rotinasParceiroLogado' );
        this.setState( { rotinas: result.data } );
    }

    render() {
        return (
            <React.Fragment>
                <GridContainer justify="center">
                    <GridItem md={12}>
                        <RoutinesReactTable
                            rotinas={this.state.rotinas}
                            history={this.props.history} />
                    </GridItem>
                </GridContainer>

            </React.Fragment>
        );
    }
}

RoutinesTableComponent.propTypes = { classes: PropTypes.object };
export default withStyles( {
    infoText: { fontWeight: "300", margin: "10px 0 30px", textAlign: "center" },
    inputAdornmentIcon: { color: "#555" },
    inputAdornment: { position: "relative" }
} )( RoutinesTableComponent );