import LoginPage from "views/Pages/LoginPage";
import RoutinesTableComponent from "views/Pages/Routines/RoutinesTableComponent";
import RoutinesFormComponent from "views/Pages/Routines/RoutinesFormComponent";
import PartnersFormComponent from 'views/Pages/Partners/PartnersFormComponent';
import RegisterPage from "views/Pages/RegisterPage.js";

var dashRoutes = [
  {//Tela de Login de Parceiro
    path: "/login-admin/:deslogado?",
    name: "Login",
    component: LoginPage,
    layout: "/auth"
  },
  {
    path: "/register-page",
    name: "Registrar",
    mini: "R",
    component: RegisterPage,
    layout: "/auth"
  },
  {//Componente Tabela das Rotinas
    path: "/rotinas",
    name: "Minhas Rotinas",
    mini: "ROT",
    component: RoutinesTableComponent,
    layout: "/admin"
  },
  {//Formulário de Cadastro/Edição de Rotinas
    path: "/rotina/:id?",
    name: "Rotina",
    mini: "R",
    component: RoutinesFormComponent,
    layout: "/admin"
  },
  {//Formulário de Cadastro/Edição de Parceiros
    path: "/parceiro/:id?",
    name: "Parceiro",
    mini: "P",
    component: PartnersFormComponent,
    layout: "/admin"
  }
];
export default dashRoutes;
