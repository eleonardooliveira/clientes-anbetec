import Api from './Api';

export const TOKEN_KEY = "@abparceiros-token";

export const isAuthenticated = () => localStorage.getItem( TOKEN_KEY ) !== null;
export const getToken = () => localStorage.getItem( TOKEN_KEY );
export const isTokenValid = async () => {
    if ( localStorage.getItem( TOKEN_KEY ) === null )
        return false;

    try {
        await Api.get( `/parceiros/verifyToken` );
        return true;
    } catch ( err ) {
        localStorage.removeItem( TOKEN_KEY );
    }
    return false;
};
export const login = token => localStorage.setItem( TOKEN_KEY, token );
export const logout = () => localStorage.removeItem( TOKEN_KEY );