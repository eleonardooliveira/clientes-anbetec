import RoutinesList from "views/Pages/Routines/RoutinesList";

var dashRoutes = [
  {
    path: "/rotinas",
    name: "Rotinas",
    mini: "ROT",
    component: RoutinesList,
    layout: "/admin"
  }

];
export default dashRoutes;
