import React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "components/CustomButtons/Button.js";
import styles from "assets/jss/material-dashboard-pro-react/modalStyle.js";
import GridContainer from "components/Grid/GridContainer.js";
import InputAdornment from "@material-ui/core/InputAdornment";
import Face from "@material-ui/icons/Face";
import CustomInput from "components/CustomInput/CustomInput.js";
import SweetAlert from "react-bootstrap-sweetalert";
import GridItem from "components/Grid/GridItem.js";
import NavPills from "components/NavPills/NavPills.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Info from "@material-ui/icons/Info";
import Preco from '@material-ui/icons/AttachMoney';
import MaisInfo from '@material-ui/icons/ContactPhone';
import { formatPrice } from "services/utils";
import PhoneIcon from '@material-ui/icons/Phone';
import Api from "services/Api";
import { formatPhone } from "services/utils";


const useStyles = makeStyles( styles );
const Transition = React.forwardRef( function Transition( props, ref ) {
  return <Slide direction="down" ref={ref} {...props} />;
} );

export default function ConteudoForm( props ) {
  const classes = useStyles();
  const [modal, setModal] = React.useState( props.modal );
  const [nome, setNome] = React.useState( "" );
  const [registerNomeState, setRegisterNomeState] = React.useState( "error" );
  const [telefone, setTelefone] = React.useState( "" );
  const [registerTelefoneState, setRegisterTelefoneState] = React.useState( "error" );
  const [alert, setAlert] = React.useState( null );


  const [activePage, setActivePage] = React.useState( 0 );

  React.useEffect( () => {
    setModal( props.modal );
  }, [props] );

  const onClose = () => {
    setModal( false );
    props.modalAction( false, null );
  };

  const hideAlert = () => {
    setAlert( null );
  };

  const autoCloseAlert = () => {
    setAlert(
      <SweetAlert
        style={{ display: "block", marginTop: "-100px" }}
        title="Obrigado!"
        onConfirm={() => hideAlert()}
        showConfirm={false}
      >
        Cadastro de Interesse realizado com sucesso!
      </SweetAlert>
    );
    setTimeout( hideAlert, 2000 );
  };

  const onSendEmailAndClose = () => {
    if ( !nome ) {
      setRegisterNomeState( "error" );
    } else {
      setRegisterNomeState( "success" );
    }

    if ( !telefone ) {
      setRegisterTelefoneState( "error" );
    } else {
      setRegisterTelefoneState( "success" );
    }

    if ( registerNomeState === "error" || registerTelefoneState === "error" ) {
      return;
    }

    const nomeRotina = props.rotina.nomeRotina;

    Api.put( '/rotinas/emailInteresse', JSON.stringify( { nome, telefone, nomeRotina } ) );

    setModal( false );
    props.modalAction( false, null );
    autoCloseAlert();
  };

  return (

    <GridContainer justify="center" spacing={1}>
      <GridItem xs={12} className={classes.center} >
        {alert}
        <Dialog
          classes={{ paper: classes.modal }}
          open={modal}
          transition={Transition}
          keepMounted
          scroll={'body'}
          onClose={() => onClose()}
          aria-labelledby="modal-slide-title"
          aria-describedby="modal-slide-description">
          <DialogContent id="modal-slide-description" className={classes.modalBody}>
            <GridContainer justify="center">
              <GridItem >
                <h3 className={classes.pageSubcategoriesTitle} >{props.rotina.nomeRotina}</h3>
                <h5 className={classes.pageSubcategoriesTitle}>Módulo: {props.rotina.modulo}</h5>
                <br />
                <NavPills
                  color="warning"
                  alignCenter
                  tabs={[
                    {
                      tabButton: "Descrição",
                      tabIcon: Info,
                      tabContent: (
                        <div>
                          <Card>
                            <CardHeader>
                              <h4 className={classes.cardTitle}>Descrição Resumida</h4>
                            </CardHeader>
                            <CardBody>
                              {props.rotina.descricaoRotina}
                            </CardBody>
                          </Card>
                          <Card>
                            <CardHeader>
                              <h4 className={classes.cardTitle}>Descrição Completa</h4>
                            </CardHeader>
                            <CardBody>
                              {props.rotina.descricaoCompletaRotina}
                            </CardBody>
                          </Card>
                        </div>
                      )
                    },
                    {
                      tabButton: "Preço",
                      tabIcon: Preco,
                      tabContent: (
                        <div>
                          {props.rotina.vendaMensal ?
                            <Card>
                              <CardHeader>
                                <h4 className={classes.cardTitle}>Mensal - {formatPrice( props.rotina.valorMensal )}</h4>
                              </CardHeader>
                            </Card> : null}
                          {props.rotina.vendaVitalicio ? <Card>
                            <CardHeader>
                              <h4 className={classes.cardTitle}>Vitalício - {formatPrice( props.rotina.valorVitalicio )}</h4>
                            </CardHeader>
                          </Card> : null}
                        </div>
                      )
                    },
                    {
                      tabButton: "Interessado?",
                      tabIcon: MaisInfo,
                      tabContent: (
                        <Card>
                          <CardHeader>
                            <h4 className={classes.cardTitle}>
                              Interessado na Rotina? <br />
                            Cadastre-se abaixo e entramos em contato com você!
                      </h4>
                          </CardHeader>
                          <CardBody>
                            <CustomInput
                              success={registerNomeState === "success"}
                              error={registerNomeState === "error"}
                              formControlProps={{
                                fullWidth: true,
                                className: classes.customFormControlClasses
                              }}
                              inputProps={{
                                startAdornment: (
                                  <InputAdornment
                                    position="start"
                                    className={classes.inputAdornment}
                                  >
                                    <Face className={classes.inputAdornmentIcon} />
                                  </InputAdornment>
                                ),
                                onChange: event => {
                                  if ( event.target.value.trim().length >= 6 ) {
                                    setRegisterNomeState( "success" );
                                  } else {
                                    setRegisterNomeState( "error" );
                                  }
                                  setNome( event.target.value );
                                },
                                placeholder: "Nome Completo..."
                              }}
                            />
                            <CustomInput
                              success={registerTelefoneState === "success"}
                              error={registerTelefoneState === "error"}
                              formControlProps={{
                                fullWidth: true,
                                className: classes.customFormControlClasses
                              }}
                              inputProps={{
                                startAdornment: (
                                  <InputAdornment
                                    position="start"
                                    className={classes.inputAdornment}
                                  >
                                    <PhoneIcon className={classes.inputAdornmentIcon} />
                                  </InputAdornment>
                                ), onChange: event => {
                                  event.target.value = formatPhone( event.target.value );

                                  if ( event.target.value.trim().length >= 14 ) {
                                    setRegisterTelefoneState( "success" );
                                  } else {
                                    setRegisterTelefoneState( "error" );
                                  }

                                  setTelefone( event.target.value );
                                },
                                placeholder: "Telefone..."
                              }}
                            />
                            <div className={classes.center}>
                              <Button
                                round
                                color="primary"
                                block
                                onClick={onSendEmailAndClose}>
                                Let{"'"}s Go
                      </Button>
                            </div>
                          </CardBody>
                        </Card>
                      )
                    }
                  ]}
                />
              </GridItem>
            </GridContainer>
          </DialogContent>

          <DialogActions className={classes.modalFooter + " " + classes.modalFooterCenter}>
            {activePage === 0 ? ( <Button onClick={() => onClose()} color="success">Fechar</Button> ) : null}
          </DialogActions>
        </Dialog>
      </GridItem>
    </GridContainer>
  );
}
