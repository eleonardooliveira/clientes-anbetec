import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Search from '@material-ui/icons/Search';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import ConteudoForm from './ConteudoForm.js';
import styles from "assets/jss/material-dashboard-pro-react/views/pricingPageStyle.js";
import { formatPrice } from "services/utils";
import Api from '../../../services/Api';
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import { relativeTimeRounding } from "moment";

const ordenacoesTodas = [
    "Relevância",
    "Novas primeiro",
    "Antigas primeiro",
    "Menor valor Mensal primeiro",
    "Maior valor Mensal primeiro",
    "Menor valor Vitalício primeiro",
    "Maior valor Vitalício primeiro"
];

const ordenacoesVendaMensal = [
    "Relevância",
    "Novas primeiro",
    "Antigas primeiro",
    "Menor valor Mensal primeiro",
    "Maior valor Mensal primeiro"
];

const ordenacoesVendaVitalicia = [
    "Relevância",
    "Novas primeiro",
    "Antigas primeiro",
    "Menor valor Vitalício primeiro",
    "Maior valor Vitalício primeiro"
];

class PartnersTableComponent extends React.Component {

    constructor( props ) {
        super( props );
        this.state = {
            rotinas: [],
            ordenacoes: ordenacoesTodas,
            filtros: [
                "Todas",
                "Apenas Venda Mensal",
                "Apenas Venda Vitalícia"
            ],
            termoPesquisa: '',
            termoPesquisado: '',
            ordenar: 'Relevância',
            filtrar: 'Todas',
            modal: false,
            rotina: {},
            queryModulo: '',
            dono: ''
        };
    }

    async componentDidMount() {
        console.log( 'mounted' );

        const result = await Api.get( `/rotinas/getTodasRotinasVitrine` );
        this.setState( {
            rotinas: result.data,
            querymodulo: 'Todas'
        } );
    }

    async componentDidUpdate() {
        console.log( 'updated' );
        const arrayUrl = this.props.history.location.pathname.split( '/' );

        if ( arrayUrl[3] ) {//está em algum módulo
            const querymodulo = arrayUrl[3].replace( ':', '' );
            const dono = arrayUrl[4].replace( ':', '' );
            console.log( "updated - " + querymodulo + " / " + dono );

            if ( this.state.querymodulo !== querymodulo || this.state.dono !== dono ) {//faz para não entrar em loop infinito
                console.log( 'if' );
                const result = await Api.get( `/rotinas/getRotinasVitrine/?dono=${ dono }&ordenar=${ this.state.ordenar.trim() }&filtrar=${ this.state.filtrar.trim() }&modulo=${ querymodulo }` );
                this.setState( {
                    rotinas: result.data,
                    querymodulo: querymodulo,
                    dono: dono
                } );
            }

        } else if ( this.state.querymodulo !== 'Todas' ) {
            console.log( 'else' );
            const result = await Api.get( `/rotinas/getTodasRotinasVitrine` );
            this.setState( {
                rotinas: result.data,
                querymodulo: 'Todas'
            } );
        }
    }

    handleModal( arg, data ) {
        this.setState( { modal: arg } );
    }

    handleForm( rotina ) {
        this.setState( {
            modal: true,
            rotina: {
                id: rotina._id,
                nomeRotina: rotina.nomeRotina,
                modulo: rotina.modulo,
                vendaMensal: rotina.vendaMensal,
                vendaVitalicio: rotina.vendaVitalicio,
                valorMensal: rotina.valorMensal,
                valorVitalicio: rotina.valorVitalicio,
                descricaoRotina: rotina.descricaoRotina,
                descricaoCompletaRotina: rotina.descricaoCompletaRotina,
            }
        } );
    }

    clickSeeMore( rotina ) {
        const idRotina = rotina._id;
        Api.put( '/rotinas/seeMore', JSON.stringify( { idRotina } ) );
    }

    async changeFilter( selectedItem ) {

        if ( selectedItem === "Apenas Venda Mensal" ) {//se quiser apenas rotinas com venda mensal
            if ( this.state.ordenar === "Menor valor Vitalício primeiro" || this.state.ordenar === "Maior valor Vitalício primeiro" ) {
                this.setState( {
                    ordenacoes: ordenacoesVendaMensal, //coloca ordenações que fazem sentido nesse contexto
                    filtrar: selectedItem,
                    ordenar: "Relevância"
                } );
                this.search( "Relevância", selectedItem );
            } else {
                this.setState( {
                    ordenacoes: ordenacoesVendaMensal, //coloca ordenações que fazem sentido nesse contexto
                    filtrar: selectedItem,
                } );
                this.search( this.state.ordenar, selectedItem );
            }
        } else if ( selectedItem === "Apenas Venda Vitalícia" ) {
            if ( this.state.ordenar === "Menor valor Mensal primeiro" || this.state.ordenar === "Maior valor Mensal primeiro" ) {
                this.setState( {
                    ordenacoes: ordenacoesVendaMensal, //coloca ordenações que fazem sentido nesse contexto
                    filtrar: selectedItem,
                    ordenar: "Relevância"
                } );
                this.search( "Relevância", selectedItem );
            } else {
                this.setState( {
                    ordenacoes: ordenacoesVendaVitalicia, //coloca ordenações que fazem sentido nesse contexto
                    filtrar: selectedItem,
                } );
                this.search( this.state.ordenar, selectedItem );
            }
        } else {
            if ( this.state.ordenar === "Menor valor Mensal primeiro" || this.state.ordenar === "Maior valor Mensal primeiro" ||
                this.state.ordenar === "Menor valor Vitalício primeiro" || this.state.ordenar === "Maior valor Vitalício primeiro"
            ) {//essas ordenações são específicas dos filtros de venda mensal ou vitalícia
                this.setState( {
                    ordenacoes: ordenacoesTodas,
                    filtrar: selectedItem,
                    ordenar: "Relevância"
                } );

                this.search( "Relevância", selectedItem );
            } else {
                this.setState( {
                    ordenacoes: ordenacoesTodas,
                    filtrar: selectedItem
                } );

                this.search( this.state.ordenar, selectedItem );
            }
        }
    }

    async changeOrder( selectedItem ) {

        if ( selectedItem === "Menor valor Mensal primeiro" || selectedItem === "Maior valor Mensal primeiro" ) {
            this.setState( {
                ordenar: selectedItem,
                filtrar: "Apenas Venda Mensal"
            } );

            this.search( selectedItem, "Apenas Venda Mensal" );

        } else if ( selectedItem === "Menor valor Vitalício primeiro" || selectedItem === "Maior valor Vitalício primeiro" ) {
            this.setState( {
                ordenar: selectedItem,
                filtrar: "Apenas Venda Vitalícia"
            } );

            this.search( selectedItem, "Apenas Venda Vitalícia" );
        } else {
            this.setState( {
                ordenar: selectedItem,
            } );

            this.search( selectedItem, this.state.filtrar );
        }
    }

    search = async ( order, filter ) => {
        const result = await Api.get( `/rotinas/getRotinasVitrine/?dono=${ this.state.dono }&termoPesquisa=${ this.state.termoPesquisa.trim() }&ordenar=${ order }&filtrar=${ filter }&modulo=${ this.state.querymodulo }` );
        if ( this.state.termoPesquisa ) {
            this.setState( {
                termoPesquisado: this.state.termoPesquisa
            } );
        }
        this.setState( {
            rotinas: result.data
        } );
    };

    render() {
        const classes = makeStyles( styles );
        return (
            <React.Fragment>
                <GridContainer justify="center" >
                    <GridItem xs={12} >
                        <Card>
                            <CardHeader color="rose" icon>
                                <CardIcon color="rose">
                                    <Search />
                                </CardIcon>
                                <h4 style={{ color: '#3C4858' }}>Pesquisa, Filtragem e Ordenação</h4>
                            </CardHeader>
                            <CardBody style={{ height: '100px' }}>
                                <GridContainer>
                                    <GridItem sm={8} lg={5}>
                                        <CustomInput
                                            formControlProps={{
                                                className: classes.top + " " + classes.search,
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                placeholder: "Pesquisar",
                                                onChange: event => {
                                                    this.setState( {
                                                        termoPesquisa: event.target.value
                                                    } );
                                                },
                                                inputProps: {
                                                    "aria-label": "Pesquisar",
                                                    className: classes.searchInput
                                                }
                                            }}
                                        />
                                    </GridItem>
                                    <Button
                                        color="white"
                                        aria-label="edit"
                                        justIcon
                                        round
                                        className={classes.top + " " + classes.searchButton + " "}
                                        onClick={() => {
                                            this.search();
                                        }}>
                                        <Search className={classes.headerLinksSvg + " " + classes.searchIcon} />
                                    </Button>
                                    <GridItem sm={6} lg={3}>
                                        <CustomDropdown
                                            buttonText={this.state.filtrar}
                                            dropdownHeader="Filtrar por"
                                            hoverColor="info"
                                            onClick={selectedItem => { this.changeFilter( selectedItem ); }}
                                            buttonProps={{
                                                round: true,
                                                block: true,
                                                color: "info"
                                            }}
                                            dropPlacement="bottom"
                                            dropdownList={this.state.filtros}
                                        />
                                    </GridItem>
                                    <GridItem sm={6} lg={3}>
                                        <CustomDropdown
                                            buttonText={this.state.ordenar}
                                            dropdownHeader="Ordenar por"
                                            hoverColor="info"
                                            onClick={selectedItem => { this.changeOrder( selectedItem ); }}
                                            buttonProps={{
                                                round: true,
                                                block: true,
                                                color: "info"
                                            }}
                                            dropPlacement="bottom"
                                            dropdownList={this.state.ordenacoes}
                                        />
                                    </GridItem>
                                </GridContainer>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>
                <GridContainer>
                    {this.state.rotinas.map( ( rotina ) =>
                        <GridItem xs={12} sm={12} md={3} key={rotina._id} >
                            <Card pricing raised>
                                <CardBody pricing style={{ height: '300px' }}>
                                    <h5 className={classes.cardCategory}>{rotina.nomeRotina}</h5>
                                    {rotina.vendaMensal ? <h6 className={`${ classes.cardTitle } ${ classes.marginTop30 }`}>
                                        Mensal: R$ {formatPrice( rotina.valorMensal )}
                                    </h6> : null}
                                    {rotina.vendaVitalicio ? <h6 className={`${ classes.cardTitle } ${ classes.marginTop30 }`}>
                                        Vitalício: R$ {formatPrice( rotina.valorVitalicio )}
                                    </h6> : null}
                                    <p className={classes.cardDescription} >
                                        {rotina.descricaoRotina}
                                    </p>
                                    <Button round color="rose"
                                        style={{ position: 'absolute', bottom: '10px', right: '30%' }}
                                        onClick={() => {
                                            this.handleForm( rotina );
                                            this.clickSeeMore( rotina );
                                        }}>
                                        Ver Mais
                                    </Button>
                                </CardBody>
                            </Card>
                        </GridItem>
                    )}
                </GridContainer>

                {/* FORMULARIO CADASTRO/EDICAO */}
                <ConteudoForm
                    modal={this.state.modal}
                    modalAction={this.handleModal.bind( this )}
                    rotina={this.state.rotina} />
            </React.Fragment >
        );
    }
}

PartnersTableComponent.propTypes = { classes: PropTypes.object };
export default withStyles( {
    infoText: { fontWeight: "300", margin: "10px 0 30px", textAlign: "center" },
    inputAdornmentIcon: { color: "#555" },
    inputAdornment: { position: "relative" }
} )( PartnersTableComponent );
