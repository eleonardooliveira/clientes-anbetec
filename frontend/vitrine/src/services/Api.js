import axios from 'axios';

const Api = axios.create( { baseURL: 'http://localhost:7000/api' } );

Api.interceptors.request.use( async config => {
    config.headers['Content-type'] = 'application/json';

    return config;
} );

export default Api; 