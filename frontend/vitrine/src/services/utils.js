export function formatDate( date ) {
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date( date ).toLocaleDateString( [], options );
}

export function formatDateHour( time ) {
    var options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };
    return new Date( time ).toLocaleDateString( [], options );
}

export function formateSize( bytes ) {
    var options = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if ( bytes === 0 ) return '0 Byte';
    var i = parseInt( Math.floor( Math.log( bytes ) / Math.log( 1024 ) ) );
    return Math.round( bytes / Math.pow( 1024, i ), 2 ) + ' ' + options[i];
}

export function formatPhone( phone ) {
    phone = phone.replace( /\D/g, "" );             //Remove tudo o que não é dígito
    phone = phone.replace( /^(\d{2})(\d)/g, "($1) $2" ); //Coloca parênteses em volta dos dois primeiros dígitos
    phone = phone.replace( /(\d)(\d{8})$/, "$1 $2" );    //Coloca hífen entre o quarto e o quinto dígitos
    phone = phone.replace( /(\d)(\d{4})$/, "$1-$2" );    //Coloca hífen entre o quarto e o quinto dígitos
    return phone;
}

export function formatPrice( price ) {
    price = price.replace( /\D/g, '' );
    price = ( price / 100 ).toFixed( 2 ) + '';
    price = price.replace( ".", "," );
    price = price.replace( /(\d)(\d{3})(\d{3}),/g, "$1.$2.$3," );
    price = price.replace( /(\d)(\d{3}),/g, "$1.$2," );
    return price;
}