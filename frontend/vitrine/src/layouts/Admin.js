import React from "react";
import cx from "classnames";
import { Switch, Route, Redirect } from "react-router-dom";
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import { makeStyles } from "@material-ui/core/styles";
import AdminNavbar from "components/Navbars/AdminNavbar.js";
import Footer from "components/Footer/Footer.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import Api from '../services/Api';
import styles from "assets/jss/material-dashboard-pro-react/layouts/adminStyle.js";
import RoutinesList from "views/Pages/Routines/RoutinesList";
import BusinessIcon from '@material-ui/icons/Business';
import BubbleChartIcon from '@material-ui/icons/BubbleChart';

var ps;

const useStyles = makeStyles( styles );

export default function Dashboard( props ) {

  const { ...rest } = props;
  const [routes, setRoutes] = React.useState( [] );
  const [mobileOpen, setMobileOpen] = React.useState( false );
  const [miniActive, setMiniActive] = React.useState( false );
  const [image] = React.useState( require( "assets/img/sidebar-2.jpg" ) );
  const [color] = React.useState( "blue" );
  const [bgColor] = React.useState( "black" );
  const [logo] = React.useState( require( "assets/img/ab-logo-w.png" ) );
  const classes = useStyles();
  const mainPanelClasses =
    classes.mainPanel +
    " " +
    cx( {
      [classes.mainPanelSidebarMini]: miniActive,
      [classes.mainPanelWithPerfectScrollbar]:
        navigator.platform.indexOf( "Win" ) > -1
    } );
  const mainPanel = React.createRef();

  const mountRoute = ( name, icon, data ) => {
    var dono = name === "Rotinas Anbetec" ? "Anbetec" : "Parceiros";
    return {
      collapse: true,
      name: name,
      icon: icon,
      state: `${ name.toLowerCase() }Collapse`,
      views: data.map( modulo => ( {
        path: `/rotinas/:${ modulo }/${ dono }`,
        name: modulo,
        mini: `-`,
        component: RoutinesList,
        layout: '/admin'
      } ) )
    };
  };

  React.useEffect( () => {
    const fetchData = async () => {

      const dashRoutes = [
        {
          path: "/rotinas",
          name: "Rotinas",
          mini: "ROT",
          component: RoutinesList,
          layout: "/admin"
        }
      ];

      const todosAnbetecArray = ["Todas Anbetec"];
      var response = await Api.get( `/rotinas/modulosAnbetec` );
      const routesAnbetec = [mountRoute( "Rotinas Anbetec", BusinessIcon, [...todosAnbetecArray, ...response.data] )];

      const todosParceirosArray = ["Todas de Parceiros"];
      response = await Api.get( `/rotinas/modulosParceiros` );
      const routesPartners = [mountRoute( "Rotinas de Parceiros", BubbleChartIcon, [...todosParceirosArray, ...response.data] )];

      setRoutes( [...dashRoutes, ...routesAnbetec, ...routesPartners] );
    };

    fetchData();
  }, [props.routes] );

  React.useEffect( () => {
    if ( navigator.platform.indexOf( "Win" ) > -1 ) {
      ps = new PerfectScrollbar( mainPanel.current, {
        suppressScrollX: true,
        suppressScrollY: false
      } );
      document.body.style.overflow = "hidden";
    }
    window.addEventListener( "resize", resizeFunction );

    // Specify how to clean up after this effect:
    return function cleanup() {
      if ( navigator.platform.indexOf( "Win" ) > -1 ) {
        ps.destroy();
      }
      window.removeEventListener( "resize", resizeFunction );
    };
  } );
  const handleDrawerToggle = () => {
    setMobileOpen( !mobileOpen );
  };
  const getRoutes = routes => {
    return routes.map( ( prop, key ) => {
      if ( prop.collapse ) {
        return getRoutes( prop.views );
      }
      if ( prop.layout === "/admin" ) {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    } );
  };
  const sidebarMinimize = () => {
    setMiniActive( !miniActive );
  };
  const resizeFunction = () => {
    if ( window.innerWidth >= 960 ) {
      setMobileOpen( false );
    }
  };

  return (
    <div className={classes.wrapper}>
      <Sidebar
        routes={routes}
        logoText={"Vitrine de Rotinas"}
        logo={logo}
        image={image}
        handleDrawerToggle={handleDrawerToggle}
        open={mobileOpen}
        color={color}
        bgColor={bgColor}
        miniActive={miniActive}
        {...rest}
      />
      <div className={mainPanelClasses} ref={mainPanel}>
        <AdminNavbar
          sidebarMinimize={sidebarMinimize.bind( this )}
          miniActive={miniActive}
          handleDrawerToggle={handleDrawerToggle}
          {...rest}
        />
        <div className={classes.content}>
          <div className={classes.container}>
            <Switch>
              {getRoutes( routes )}
              <Redirect from="/admin" to="/admin/rotinas" />
            </Switch>
          </div>
        </div>
        <Footer fluid />
      </div>
    </div>
  );
};
