import React from 'react';

import ReactTable from "react-table";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import SweetAlert from "react-bootstrap-sweetalert";

// @material-ui/icons
import Assignment from "@material-ui/icons/Assignment";
import Close from "@material-ui/icons/Close";

import Api from '../../../services/Api';

import { Link } from 'react-router-dom';

// core components
import GridContainer from "components/Grid/GridContainer.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import GridItem from "components/Grid/GridItem.js";
import CardBody from "components/Card/CardBody.js";
import CardIcon from "components/Card/CardIcon.js";
import CardHeader from "components/Card/CardHeader.js";


import { cardTitle } from "assets/jss/material-dashboard-pro-react.js";

import { formatDate } from 'services/utils';

const styles = {
    cardIconTitle: {
        ...cardTitle,
        marginTop: "15px",
        marginBottom: "0px"
    }
};

const useStyles = makeStyles( styles );

export default function AdministratorsReactTable( props ) {
    const [administrador, setAdministrador] = React.useState( props.administrador );
    const [alert, setAlert] = React.useState( null );
    React.useEffect( () => setAdministrador( props.administrador ), [props.administrador] );

    const classes = useStyles();

    const openAlert = ( { type, administrador } ) => {
        if ( type === 'delete' )
            setAlert(
                <SweetAlert
                    danger
                    style={{ display: "block", marginTop: "-200px" }}
                    showCancel={true}
                    title="Confirmação"
                    confirmBtnText={'Confirmar'}
                    cancelBtnText={'Cancelar'}
                    onConfirm={async () => {

                        var newAdministrador = administrador;
                        for ( var i = 0; i < administrador.length; i++ ) {

                            if ( administrador[i]._id === administrador._id ) {
                                //excluindo do banco
                                const res = await Api.delete( `/administradores/${ administrador._id }` );

                                if ( res.status === 200 ) {
                                    //excluindo da tela
                                    newAdministrador.splice( i, 1 );
                                    setAdministrador( [...newAdministrador] );
                                }
                                break;
                            }

                        }
                        setAlert( null );
                    }}
                    onCancel={() => {
                        setAlert( null );
                    }}
                    cancelBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 MuiButton-text`}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
                    Deseja realmente excluir o Administrador '<b>{administrador.nomeAdministrador}</b>'' ?
          </SweetAlert > );

    };

    return (
        <GridContainer>
            <GridItem md={12}>
                {/* CX. DIALOGO */}
                <div>{alert}</div>
            </GridItem>
            <GridItem xs={12}>
                <div style={{ display: "flex" }}>
                    <Button
                        xs={3}
                        style={{ marginLeft: "auto" }}
                        color="info"
                        onClick={() => {
                            props.history.push( '/admin/administrador' );
                        }}>
                        Cadastrar Administrador
                     </Button>
                </div>
                <Card>
                    <CardHeader color="primary" icon>
                        <CardIcon color="primary">
                            <Assignment />
                        </CardIcon>
                        <h4 className={classes.cardIconTitle}>Administradores</h4>
                    </CardHeader>
                    <CardBody>
                        <ReactTable
                            data={
                                administrador.map( administrador => {
                                    return ( {
                                        id: administrador._id,
                                        nome: administrador.nomeAdministrador,
                                        email: administrador.email,
                                        data_registro: ( formatDate( administrador.data_registro ) ),
                                        isAtivo: administrador.isAtivo === true ? "Sim" : "Não",
                                        actions: (
                                            <div className="actions-right">
                                                <Link to={`/admin/administrador/${ administrador._id }`} className="btn btn-primary" > Editar</Link>
                                                <Button
                                                    justIcon
                                                    round
                                                    simple
                                                    onClick={() => {
                                                        openAlert( { type: 'delete', administrador: administrador } );
                                                    }}
                                                    color="danger"
                                                    className="remove"
                                                >
                                                    <Close />
                                                </Button>{" "}</div>
                                        )
                                    } );
                                } )
                            }
                            columns={[
                                {
                                    Header: "Nome",
                                    accessor: "nome",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "E-mail",
                                    accessor: "email",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "Data de Cadastro",
                                    accessor: "data_registro",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "Ativo",
                                    accessor: "isAtivo",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "Ações",
                                    accessor: "actions",
                                    sortable: false,
                                    filterable: false,
                                    resizable: false
                                }
                            ]}
                            defaultPageSize={( administrador.length === 0 ) ? 5 : administrador.length}
                            minRows={1}
                            showPaginationTop
                            showPaginationBottom={false}
                            nextText={'Próximo'}
                            previousText={'Anterior'}
                            rowsText={'administradores'}
                            pageText={'Página'}
                            ofText={'de'}
                            loadingText='Carregando...'
                            noDataText='Nenhum Administrador encontrado'
                            className="-striped -highlight"
                        />

                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
    );
};