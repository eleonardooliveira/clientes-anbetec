import React from "react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
import SweetAlert from "react-bootstrap-sweetalert";

// @material-ui/icons
import Timeline from "@material-ui/icons/Timeline";
import Code from "@material-ui/icons/Code";
import Group from "@material-ui/icons/Group";
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import InfoArea from "components/InfoArea/InfoArea.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";

import Api from "services/Api";
import { verifyEmail } from "services/validations";

import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";

const useStyles = makeStyles( styles );

export default function RegisterPage( props ) {
  const classes = useStyles();

  const [nomeAdministrador, setNomeAdministrador] = React.useState( "" );
  const [registerNomeAdministradortate, setRegisterNomeAdministradortate] = React.useState( "" );
  const [email, setEmail] = React.useState( "" );
  const [registerEmailState, setRegisterEmailState] = React.useState( "" );
  const [senha, setSenha] = React.useState( "" );
  const [registerSenhaState, setRegisterSenhaState] = React.useState( "" );
  const [alert, setAlert] = React.useState( null );

  const handleRegister = async e => {
    //Request Body
    const body = JSON.stringify( { nomeAdministrador, email, senha } );

    if ( nomeAdministrador && email && senha &&
      registerNomeAdministradortate === "success" && registerEmailState === "success" && registerSenhaState === "success" ) {
      try {
        const res = await Api.post( '/administrador', body );

        if ( res.data.status === "OK" )
          openAlert( { redirect: true, message: 'Registro realizado com sucesso, solicite a um Administrador a liberação do seu acesso!' } );
        else
          openAlert( { redirect: false, message: res.data.status } );
      } catch ( err ) {
        openAlert( { redirect: false, message: err.response.data.msg } );
      }
    } else {
      if ( !nomeAdministrador ) {
        openAlert( { redirect: false, message: 'Insira seu Nome Completo!' } );
      } else if ( registerNomeAdministradortate === "error" ) {
        openAlert( { redirect: false, message: 'Nome inválido, digite pelo menos 6 letras!' } );
      } else if ( !email ) {
        openAlert( { redirect: false, message: 'Insira seu E-mail!' } );
      } else if ( registerEmailState === "error" ) {
        openAlert( { redirect: false, message: 'E-mail inválido!' } );
      } else if ( !senha ) {
        openAlert( { redirect: false, message: 'Insira sua Senha!' } );
      } else if ( registerSenhaState === "error" ) {
        openAlert( { redirect: false, message: 'Senha inválida, digite pelo menos 6 caracteres!' } );
      }
    }
  };

  const openAlert = ( { redirect, message } ) => {

    if ( redirect ) {
      setAlert(
        <SweetAlert
          success
          style={{ display: "block", marginTop: "-200px" }}
          showCancel={false}
          title={message}
          confirmBtnText={'Certo!'}
          onConfirm={async () => {
            if ( redirect )
              props.history.push( "/auth" );

            setAlert( null );
          }}
          confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>

        </SweetAlert > );
    } else {
      setAlert(
        <SweetAlert
          danger
          style={{ display: "block", marginTop: "-200px" }}
          showCancel={false}
          title={message}
          confirmBtnText={'Certo!'}
          onConfirm={async () => {
            if ( redirect )
              props.history.push( "/auth" );

            setAlert( null );
          }}
          confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>

        </SweetAlert > );
    }

  };

  return (
    <div className={classes.container}>
      {/* CX. DIALOGO */}
      <div>{alert}</div>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={10}>
          <Card className={classes.cardSignup}>
            <h2 className={classes.cardTitle}>Registrar</h2>
            <CardBody>
              <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={5}>
                  <InfoArea
                    title="Marketing"
                    description="We've created the marketing campaign of the website. It was a very interesting collaboration."
                    icon={Timeline}
                    iconColor="rose"
                  />
                  <InfoArea
                    title="Fully Coded in HTML5"
                    description="We've developed the website with HTML5 and CSS3. The client has access to the code using GitHub."
                    icon={Code}
                    iconColor="primary"
                  />
                  <InfoArea
                    title="Built Audience"
                    description="There is also a Fully Customizable CMS Admin Dashboard for this product."
                    icon={Group}
                    iconColor="info"
                  />
                </GridItem>
                <GridItem xs={12} sm={8} md={5}>
                  <form className={classes.form}>
                    <CustomInput
                      success={registerNomeAdministradortate === "success"}
                      error={registerNomeAdministradortate === "error"}
                      formControlProps={{
                        fullWidth: true,
                        className: classes.customFormControlClasses
                      }}
                      inputProps={{
                        startAdornment: (
                          <InputAdornment
                            position="start"
                            className={classes.inputAdornment}
                          >
                            <Face className={classes.inputAdornmentIcon} />
                          </InputAdornment>
                        ),
                        onChange: event => {
                          if ( event.target.value.trim().length >= 6 ) {
                            setRegisterNomeAdministradortate( "success" );
                          } else {
                            setRegisterNomeAdministradortate( "error" );
                          }
                          setNomeAdministrador( event.target.value );
                        },
                        placeholder: "Nome Completo..."
                      }}
                    />
                    <CustomInput
                      success={registerEmailState === "success"}
                      error={registerEmailState === "error"}
                      formControlProps={{
                        fullWidth: true,
                        className: classes.customFormControlClasses
                      }}
                      inputProps={{
                        startAdornment: (
                          <InputAdornment
                            position="start"
                            className={classes.inputAdornment}
                          >
                            <Email className={classes.inputAdornmentIcon} />
                          </InputAdornment>
                        ), onChange: event => {
                          if ( verifyEmail( event.target.value ) ) {
                            setRegisterEmailState( "success" );
                          } else {
                            setRegisterEmailState( "error" );
                          }
                          setEmail( event.target.value );
                        },
                        placeholder: "Email..."
                      }}
                    />
                    <CustomInput
                      success={registerSenhaState === "success"}
                      error={registerSenhaState === "error"}
                      formControlProps={{
                        fullWidth: true,
                        className: classes.customFormControlClasses
                      }}
                      inputProps={{
                        startAdornment: (
                          <InputAdornment
                            position="start"
                            className={classes.inputAdornment}
                          >
                            <Icon className={classes.inputAdornmentIcon}>
                              lock_outline
                            </Icon>
                          </InputAdornment>
                        ), onChange: event => {
                          if ( event.target.value.trim().length >= 6 ) {
                            setRegisterSenhaState( "success" );
                          } else {
                            setRegisterSenhaState( "error" );
                          }
                          setSenha( event.target.value );
                        },
                        type: "password",
                        placeholder: "Senha..."
                      }}
                    />

                    <div className={classes.center}>
                      <Button
                        round
                        color="primary"
                        block
                        onClick={handleRegister}>
                        Let{"'"}s Go
                      </Button>
                    </div>
                  </form>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
