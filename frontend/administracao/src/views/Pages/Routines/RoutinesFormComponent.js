import React from "react";

import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import withStyles from "@material-ui/core/styles/withStyles";
import AttachmentIcon from '@material-ui/icons/Attachment';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import SweetAlert from "react-bootstrap-sweetalert";
import { formatDateHour, formateSize, formatPrice } from "services/utils";
import InputAdornment from '@material-ui/core/InputAdornment';

import { Link } from 'react-router-dom';

import Card from "components/Card/Card.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardHeader from "components/Card/CardHeader.js";
import CardText from "components/Card/CardText.js";
import CardBody from "components/Card/CardBody.js";

import Api from '../../../services/Api';

import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import styles from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.js";
import './RoutinesFormComponent.css';

import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';

class RoutinesFormComponent extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            id: '',
            name: '',
            descricaoRotina: '',
            descricaoCompletaRotina: '',
            modulo: '',
            checkAtivo: true,
            isUpdate: false,
            inputNameState: "",
            inputDescricaoState: "",
            inputDescricaoCompletaState: "",
            inputValorMensalState: "",
            inputValorVitalicioState: "",
            alert: null,
            files: [],
            vendaMensal: false,
            vendaVitalicio: false,
            valorMensal: '',
            valorVitalicio: '',
            arquivosSalvos: 0,
            arquivos: [],
            arquivosNovos: [],
            uploadArchivesStatus: 'Carregando arquivos...'
        };
    }

    async componentDidMount() {
        const { id } = this.props.match.params;

        if ( id !== ":id" && id !== undefined ) {
            const result = await Api.get( `/rotinas/?idRotina=${ id }` );

            const resultArquivos = result.data.arquivos;

            resultArquivos.forEach( async ( id, index ) => {
                const resultFile = await Api.get( `arquivos/?idArquivo=${ resultArquivos[index] }` );

                var arrayAux = this.state.arquivos;
                arrayAux.push( resultFile.data );

                this.setState( {
                    arquivos: arrayAux
                } );

            } );

            const resultDono = await Api.get( `/rotinas/donoInfos/?idParceiroDono=${ result.data.idParceiroDono }` );

            var nameOwner = '';
            if ( resultDono.data.nomeParceiro ) {
                nameOwner = resultDono.data.nomeParceiro;
                this.setState( { ownerType: 'parceiro' } );
            } else {
                nameOwner = resultDono.data.nomeAdministrador;
                this.setState( { ownerType: 'administrador' } );
            }

            this.setState( {
                id: id,
                name: result.data.nomeRotina,
                descricaoRotina: result.data.descricaoRotina,
                descricaoCompletaRotina: result.data.descricaoCompletaRotina,
                modulo: result.data.modulo,
                checkAtivo: result.data.isAtivo,
                isUpdate: true,
                contCaracteres: result.data.descricaoRotina.trim().length + '/300',
                contCaracteresCompleta: result.data.descricaoCompletaRotina.trim().length + '/300',
                vendaMensal: result.data.vendaMensal,
                vendaVitalicio: result.data.vendaVitalicio,
                valorMensal: formatPrice( result.data.valorMensal ),
                valorVitalicio: formatPrice( result.data.valorVitalicio ),
                idParceiroDono: result.data.idParceiroDono,
                nameOwner: nameOwner
            } );

        }

    }

    onChange = ( e ) => {
        this.setState( { [e.target.name]: e.target.value } );
    };

    setValorMensalRotina = ( valueStatus ) => {
        this.setState( {
            valorMensal: valueStatus
        } );
    };

    setValorVitalicioRotina = ( valueStatus ) => {
        this.setState( {
            valorVitalicio: valueStatus
        } );
    };

    setInputNameState = ( valueStatus ) => {
        this.setState( {
            inputNameState: valueStatus
        } );
    };

    setInputDescricaoState = ( valueStatus ) => {
        this.setState( {
            inputDescricaoState: valueStatus
        } );
    };

    setInputDescricaoCompletaState = ( valueStatus ) => {
        this.setState( {
            inputDescricaoCompletaState: valueStatus
        } );
    };

    setInputValorMensalState = ( valueStatus ) => {
        this.setState( {
            inputValorMensalState: valueStatus
        } );
    };

    setInputValorVitalicioState = ( valueStatus ) => {
        this.setState( {
            inputValorVitalicioState: valueStatus
        } );
    };

    toggleCheckbox( event ) {
        switch ( event.target.value ) {
            case "checkAtivo":
                this.setState( {
                    checkAtivo: ( this.state.checkAtivo === "on" || this.state.checkAtivo === true ) ? false : true
                } );
                break;
            case "vendaMensal":
                this.setState( {
                    vendaMensal: ( this.state.vendaMensal === "on" || this.state.vendaMensal === true ) ? false : true
                } );
                break;
            case "vendaVitalicio":
                this.setState( {
                    vendaVitalicio: ( this.state.vendaVitalicio === "on" || this.state.vendaVitalicio === true ) ? false : true
                } );
                break;
            default: break;
        }
    }

    setAlert( arg ) {
        this.setState( { alert: arg } );
    }

    hideAlert = () => {
        this.setAlert( null );
    };

    openAlert = ( { type, redirect, message, arquivo } ) => {

        if ( type === 'confirmDeleteArchive' ) {
            this.setAlert(
                <SweetAlert
                    danger
                    showCancel={true}
                    title="Confirmação"
                    confirmBtnText={'Confirmar'}
                    cancelBtnText={'Cancelar'}
                    onConfirm={async () => {
                        var array = [...this.state.arquivos];
                        var index = array.indexOf( arquivo );
                        if ( index !== -1 ) {
                            array.splice( index, 1 );
                            this.setState( { arquivos: array } );
                        }
                        //excluindo do banco
                        await Api.delete( `/arquivos/${ arquivo._id }` );

                        this.setAlert( null );
                    }}
                    onCancel={() => {
                        this.setAlert( null );
                    }}
                    cancelBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 MuiButton-text`}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
                    Deseja realmente excluir o Arquivo? Essa ação não pode ser desfeita!
          </SweetAlert > );
        } if ( type === 'loadUploadArchives' ) {
            this.setAlert(
                <SweetAlert
                    custom
                    customIcon=""
                    style={{ display: "block", marginTop: "-200px" }}
                    showCancel={false}
                    title={""}
                    confirmBtnText={''}
                    onConfirm={async () => { }}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
                    <img src='https://media3.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif' alt='' />
                    <br />
                    {this.state.uploadArchivesStatus}
                </SweetAlert> );
        } else {

            if ( redirect ) {
                this.setAlert(
                    <SweetAlert
                        success
                        style={{ display: "block", marginTop: "-200px" }}
                        showCancel={false}
                        title={message}
                        confirmBtnText={''}
                        onConfirm={async () => {
                            this.setAlert( null );
                        }}
                        confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
                    </SweetAlert> );
            } else {
                this.setAlert(
                    <SweetAlert
                        danger
                        style={{ display: "block", marginTop: "-200px" }}
                        showCancel={false}
                        title={message}
                        confirmBtnText={'Certo!'}
                        onConfirm={async () => {
                            this.setAlert( null );
                        }}
                        confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
                    </SweetAlert> );
            }
        }
    };

    salvar = async () => {
        try {

            if ( this.state.name.trim().length < 6 ) {
                this.openAlert( { redirect: false, message: 'Nome inválido, digite pelo menos 6 letras!' } );
                this.setInputNameState( "error" );
                return;
            }
            this.setInputNameState( "success" );

            if ( this.state.descricaoRotina.trim().length < 10 ) {
                this.openAlert( { redirect: false, message: 'Descrição inválida, digite pelo menos 10 letras!' } );
                this.setInputDescricaoState( "error" );
                return;
            }

            if ( this.state.descricaoRotina.trim().length > 300 ) {
                this.openAlert( { redirect: false, message: 'Descrição inválida, digite menos de 300 caracteres!' } );
                this.setInputDescricaoState( "error" );
                return;
            }
            this.setInputDescricaoState( "success" );

            if ( this.state.descricaoCompletaRotina.trim().length < 100 ) {
                this.openAlert( { redirect: false, message: 'Descrição Completa inválida, digite pelo menos 10 letras!' } );
                this.setInputDescricaoCompletaState( "error" );
                return;
            }

            if ( this.state.descricaoCompletaRotina.trim().length > 300 ) {
                this.openAlert( { redirect: false, message: 'Descrição Completa inválida, digite menos de 300 caracteres!' } );
                this.setInputDescricaoCompletaState( "error" );
                return;
            }
            this.setInputDescricaoCompletaState( "success" );

            if ( !this.state.modulo.trim() ) {
                this.openAlert( { redirect: false, message: 'Selecione um Módulo para a Rotina!' } );
                return;
            }

            if ( !this.state.vendaMensal && !this.state.vendaVitalicio ) {
                this.openAlert( { redirect: false, message: 'Você precisa inserir pelo menos um Valor para a Rotina!' } );
                return;
            }

            if ( this.state.vendaMensal && ( !this.state.valorMensal || this.state.valorMensal === '0,00' ) ) {
                this.openAlert( { redirect: false, message: 'Digite o valor Mensal da Rotina!' } );
                this.setInputValorMensalState( "error" );
                return;
            }

            if ( this.state.vendaVitalicio && ( !this.state.valorVitalicio || this.state.valorVitalicio === '0,00' ) ) {
                this.openAlert( { redirect: false, message: 'Digite o valor Vitalício da Rotina!' } );
                this.setInputValorVitalicioState( "error" );
                return;
            }

            if ( this.state.files.length === 0 && !this.state.isUpdate ) {//Se for novo e não tiver anexado nada...
                this.openAlert( { redirect: false, message: 'Anexe os arquivos da Rotina!' } );
                return;
            }
            this.setInputValorMensalState( "success" );
            this.setInputValorVitalicioState( "success" );


            if ( this.state.isUpdate ) {//atualizando o cadastro

                await Api.put( `/rotinas`,
                    {
                        id: this.state.id,
                        nomeRotina: this.state.name,
                        descricaoRotina: this.state.descricaoRotina,
                        descricaoCompletaRotina: this.state.descricaoCompletaRotina,
                        modulo: this.state.modulo,
                        vendaMensal: this.state.vendaMensal,
                        vendaVitalicio: this.state.vendaVitalicio,
                        valorMensal: this.state.vendaMensal ? this.state.valorMensal.replace( /\D/g, '' ) : '0',
                        valorVitalicio: this.state.vendaVitalicio ? this.state.valorVitalicio.replace( /\D/g, '' ) : '0',
                        isAtivo: this.state.checkAtivo
                    } );

                //fazendo upload de novos arquivos, se tiver
                if ( this.state.files.length > 0 ) {
                    this.openAlert( { type: 'loadUploadArchives' } );
                    await this.uploadArchives();
                } else {
                    setTimeout( () => { this.props.history.push( '/admin/rotinas' ); }, 2000 );
                    this.openAlert( { redirect: true, message: 'Cadastro salvo com sucesso!' } );
                }
            } else {//novo cadastro

                const res = await Api.post( `/rotinas`,
                    {
                        nomeRotina: this.state.name,
                        descricaoRotina: this.state.descricaoRotina,
                        descricaoCompletaRotina: this.state.descricaoCompletaRotina,
                        modulo: this.state.modulo,
                        vendaMensal: this.state.vendaMensal,
                        vendaVitalicio: this.state.vendaVitalicio,
                        valorMensal: this.state.vendaMensal ? this.state.valorMensal.replace( /\D/g, '' ) : '0',
                        valorVitalicio: this.state.vendaVitalicio ? this.state.valorVitalicio.replace( /\D/g, '' ) : '0',
                        isAtivo: this.state.checkAtivo
                    } );

                this.setState( {
                    id: res.data._id
                } );

                this.openAlert( { type: 'loadUploadArchives' } );
                await this.uploadArchives();
            }

        } catch ( err ) {
            if ( err.response )
                this.openAlert( { redirect: false, message: err.response.data.msg } );
            else
                this.openAlert( { redirect: false, message: err } );
        }
    };

    uploadArchives = async () => {
        const config = { headers: { 'content-type': 'multipart/form-data' } };

        var arr = [];

        this.state.arquivos.forEach( async ( id, index ) => {
            arr.push( this.state.arquivos[index].nomeArquivoOriginal );
        } );

        this.state.files.forEach( async ( id, index ) => {
            var formData = new FormData();

            if ( arr.indexOf( this.state.files[index].name ) === -1 ) {//não fez upload de arquivo com este nome...
                arr.push( this.state.files[index].name );

                formData.append( 'file', this.state.files[index] );
                formData.append( 'idRotina', this.state.id );
                formData.append( 'lastModified', this.state.files[index].lastModified );
                formData.append( 'size', this.state.files[index].size );

                var bla = await Api.post( `/arquivos`, formData, config );// salvando anexo
                console.log( bla );
                var contadorArquivosSalvos = this.state.arquivosSalvos + 1;

                if ( contadorArquivosSalvos === this.state.files.length ) {
                    setTimeout( () => { this.props.history.push( '/admin/rotinas' ); }, 2000 );
                    this.openAlert( { redirect: true, message: 'Cadastro salvo com sucesso!' } );
                } else {
                    this.setState( {
                        uploadArchivesStatus: 'Carregando arquivos...\t' + contadorArquivosSalvos + ' de ' + this.state.files.length,
                        arquivosSalvos: contadorArquivosSalvos
                    } );

                    this.openAlert( { type: 'loadUploadArchives' } );
                }


            }


        } );
    };

    confirmDeleteArchive = async ( arquivo ) => {

        if ( this.state.arquivos.length === 1 ) {
            this.openAlert( { redirect: false, message: 'Não é permitido deixar uma Rotina sem arquivos!' } );
            return;
        }

        this.openAlert( { type: 'confirmDeleteArchive', redirect: false, message: '', arquivo } );
    };

    render() {

        const classes = makeStyles( styles );

        const modulos = ["Compra", "Venda", "Caixa Motorista", "Processamento", "Tesouraria", "Contas a Pagar", "Ferramentas do Sistema", "Expedição", "Livros Fiscais", "Adm. Interna do Estoque", "Contas a Receber", "Recebimento de Mercadoria", "Faturamento", "Cobrança Magnética", "Indústria", "WMS", "Contas a receber do fornecedor", "Call Center", "Auto Serviço", "Contábil", "Medicamentos", "Integrações", "Comodato", "Integração com fórmula de tintas", "Rec. Mercadoria - Beneficiamento", "Rec. Mercadoria - NF Importação", "Vendas Avançado", "Controle de Consumo/Imobilizado", "Ordem de Serviço", "Reposição de Lojas", "Rádio Frequência WMS", "Rádio Frequência Winthor", "Planejamento Estratégico", "Tributação", "Integração TOTVS E-commerce", "MyMIX", "MyMIX Mobile", "Conciliação Cartão", "Relatórios Personalizados"];

        return (
            <GridContainer>
                <GridItem md={12}>
                    {/* CX. DIALOGO */}
                    <div>{this.state.alert}</div>
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose" text>
                            <CardText color="rose">
                                {this.state.isUpdate ?
                                    <h4 className={classes.cardTitle}>Edição de Rotina</h4>
                                    :
                                    <h4 className={classes.cardTitle}>Cadastro de Rotina</h4>
                                }
                            </CardText>
                        </CardHeader>
                        <CardBody>
                            <GridContainer>
                                <GridItem xs={8}>
                                    <legend>Dados da Rotina</legend>
                                    {this.state.isUpdate ?
                                        <div className={classes.block}>
                                            {this.state.ownerType === 'administrador' ?
                                                <div>
                                                    <b>Dono da Rotina: </b>Anbetec
                                                    </div>
                                                :
                                                <div>
                                                    <b>Dono da Rotina: </b>
                                                    <Link to={`/admin/parceiro/${ this.state.idParceiroDono }`} className="btn btn-pink" >
                                                        {this.state.nameOwner}
                                                    </Link>
                                                </div>
                                            }
                                        </div>
                                        :
                                        null
                                    }
                                    <CustomInput
                                        id="name"
                                        name="name"
                                        labelText={<span>Nome: <small>(obrigatório)</small></span>}
                                        success={this.state.inputNameState === "success"}
                                        error={this.state.inputNameState === "error"}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            value: this.state.name,
                                            onChange: event => {
                                                if ( event.target.value.trim().length >= 6 ) {
                                                    this.setInputNameState( "success" );
                                                } else {
                                                    this.setInputNameState( "error" );
                                                }
                                                this.setState( {
                                                    name: event.target.value
                                                } );
                                            },
                                            type: "text"
                                        }}
                                    />
                                    <CustomInput
                                        id="descricaoRotina"
                                        name="descricaoRotina"
                                        labelText={<span>Descrição: <small>(obrigatório - max. 300 caracteres)</small></span>}
                                        success={this.state.inputDescricaoState === "success"}
                                        error={this.state.inputDescricaoState === "error"}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            value: this.state.descricaoRotina,
                                            multiline: true,
                                            onChange: event => {
                                                if ( event.target.value.trim().length >= 10 && event.target.value.trim().length <= 300 ) {
                                                    this.setInputDescricaoState( "success" );
                                                } else {
                                                    this.setInputDescricaoState( "error" );
                                                }
                                                this.setState( {
                                                    descricaoRotina: event.target.value,
                                                    contCaracteres: event.target.value.trim().length + '/300'
                                                } );
                                            },
                                            type: "text"
                                        }}

                                    />
                                    {this.state.contCaracteres}
                                    <CustomInput
                                        id="descricaoCompletaRotina"
                                        name="descricaoCompletaRotina"
                                        labelText={<span>Descrição Completa: <small>(obrigatório - entre 100 e 300 caracteres)</small></span>}
                                        success={this.state.inputDescricaoCompletaState === "success"}
                                        error={this.state.inputDescricaoCompletaState === "error"}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            value: this.state.descricaoCompletaRotina,
                                            multiline: true,
                                            onChange: event => {
                                                if ( event.target.value.trim().length >= 100 && event.target.value.trim().length <= 300 ) {
                                                    this.setInputDescricaoCompletaState( "success" );
                                                } else {
                                                    this.setInputDescricaoCompletaState( "error" );
                                                }
                                                this.setState( {
                                                    descricaoCompletaRotina: event.target.value,
                                                    contCaracteresCompleta: event.target.value.trim().length + '/300'
                                                } );
                                            },
                                            type: "text"
                                        }}

                                    />
                                    {this.state.contCaracteresCompleta}
                                    <br /><br />
                                    <InputLabel
                                        htmlFor="simple-select"
                                        className={classes.selectLabel}
                                    >
                                        Selecione o Módulo da Rotina
                                    </InputLabel>
                                    <GridItem xs={12} sm={6} md={5} lg={5}></GridItem>
                                    <Select fullWidth
                                        MenuProps={{
                                            className: classes.selectMenu
                                        }}
                                        classes={{
                                            select: classes.select
                                        }}
                                        value={this.state.modulo}
                                        onChange={event => {
                                            this.setState( {
                                                modulo: event.target.value
                                            } );
                                        }}
                                        inputProps={{
                                            name: "simpleSelect",
                                            id: "simple-select"
                                        }}
                                    >
                                        {modulos.map( ( modulo, index ) =>
                                            <MenuItem
                                                key={modulo}
                                                classes={{
                                                    root: classes.selectMenuItem,
                                                    selected: classes.selectMenuItemSelected
                                                }}
                                                value={modulo}
                                            >
                                                {modulo}
                                            </MenuItem>
                                        )}
                                    </Select>
                                    <br />
                                    <br />

                                    <legend>Valor da Rotina</legend>
                                    <FormControlLabel
                                        label="Modalidade de Cobrança Mensal"
                                        control={
                                            <Switch
                                                checked={this.state.vendaMensal}
                                                onChange={this.toggleCheckbox.bind( this )}
                                                value="vendaMensal"
                                                classes={{
                                                    switchBase: classes.switchBase,
                                                    checked: classes.switchChecked,
                                                    thumb: classes.switchIcon,
                                                    track: classes.switchBar
                                                }}
                                            />
                                        }
                                        classes={{
                                            label: classes.label
                                        }}
                                    />
                                    <CustomInput
                                        id="valorMensal"
                                        name="valorMensal"
                                        labelText="Valor Mensal da Rotina"
                                        success={this.state.inputValorMensalState === "success"}
                                        error={this.state.inputValorMensalState === "error"}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            value: this.state.valorMensal,
                                            multiline: false,
                                            onChange: event => {
                                                this.setValorMensalRotina( formatPrice( event.target.value ) );
                                                if ( event.target.value !== '0' && event.target.value !== '0,0' && event.target.value !== '0,000' )
                                                    this.setInputValorMensalState( "success" );
                                                else
                                                    this.setInputValorMensalState( "error" );
                                            },
                                            startAdornment: <InputAdornment position="start">R$</InputAdornment>,
                                            type: "text",
                                            disabled: !this.state.vendaMensal
                                        }}

                                    />
                                    <FormControlLabel
                                        label="Modalidade de Cobrança Vitalício"
                                        control={
                                            <Switch
                                                checked={this.state.vendaVitalicio}
                                                onChange={this.toggleCheckbox.bind( this )}
                                                value="vendaVitalicio"
                                                classes={{
                                                    switchBase: classes.switchBase,
                                                    checked: classes.switchChecked,
                                                    thumb: classes.switchIcon,
                                                    track: classes.switchBar
                                                }}
                                            />
                                        }
                                        classes={{
                                            label: classes.label
                                        }}
                                    />
                                    <CustomInput
                                        id="valorVitalicio"
                                        name="valorVitalicio"
                                        labelText="Valor Vitalício da Rotina"
                                        success={this.state.inputValorVitalicioState === "success"}
                                        error={this.state.inputValorVitalicioState === "error"}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            value: this.state.valorVitalicio,
                                            multiline: false,
                                            onChange: event => {
                                                this.setValorVitalicioRotina( formatPrice( event.target.value ) );
                                                if ( event.target.value !== '0' && event.target.value !== '0,0' && event.target.value !== '0,000' )
                                                    this.setInputValorVitalicioState( "success" );
                                                else
                                                    this.setInputValorVitalicioState( "error" );
                                            },
                                            startAdornment: <InputAdornment position="start">R$</InputAdornment>,
                                            type: "text",
                                            disabled: !this.state.vendaVitalicio
                                        }}

                                    />
                                </GridItem>
                                <GridItem xs={4} >
                                    <legend>Controle da Rotina</legend>
                                    <div className={classes.block}>
                                        <FormControlLabel
                                            label="Rotina está Ativa?"
                                            control={
                                                <Switch
                                                    checked={this.state.checkAtivo}
                                                    onChange={this.toggleCheckbox.bind( this )}
                                                    value="checkAtivo"
                                                    classes={{
                                                        switchBase: classes.switchBase,
                                                        checked: classes.switchChecked,
                                                        thumb: classes.switchIcon,
                                                        track: classes.switchBar
                                                    }}
                                                />
                                            }
                                            classes={{
                                                label: classes.label
                                            }}
                                        />
                                    </div>
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <legend xs={8}>Arquivos</legend>
                                {this.state.isUpdate && this.state.arquivos.length > 0 ?
                                    <GridContainer
                                        justify="center"
                                        alignItems="center"

                                        direction="row">
                                        {this.state.arquivos.map( ( arquivo, index ) =>
                                            <GridItem xs={4} key={index}>
                                                <Card profile>
                                                    <CardAvatar profile>
                                                        <a href="#pablo" onClick={e => e.preventDefault()}>
                                                            <AttachmentIcon style={{ fontSize: 60 }} />
                                                        </a>
                                                    </CardAvatar>
                                                    <CardBody profile key={arquivo._id} xs={4} sm={4}>
                                                        <h4 className={classes.cardTitle}>{arquivo.nomeArquivoOriginal}</h4>
                                                        <p className={classes.description}><b>Tamanho: </b>{formateSize( arquivo.tamanho )}</p>
                                                        <p className={classes.description}><b>Tipo: </b>{arquivo.tipoArquivo}</p>
                                                        <p className={classes.description}><b>Data Última Alteração: </b>{formatDateHour( arquivo.dataUltAlteracao )}</p>
                                                        <p className={classes.description}><b>Data de Upload: </b>{formatDateHour( arquivo.dataUpload )}</p>
                                                        <Button color="rose" href={arquivo.urlDownload} download={arquivo.nomeArquivoOriginal} round>Baixar</Button>
                                                        <Button color="rose" onClick={() => this.confirmDeleteArchive( arquivo )} round>Apagar</Button>
                                                    </CardBody>
                                                </Card>
                                            </GridItem>
                                        )}
                                    </GridContainer> : null}
                                <GridItem xs={12}>
                                    <div className="Upload">
                                        <span className="Title">Anexe abaixo os arquivos da Rotina (executável, imagens, documentações etc.)</span>
                                        <FilePond
                                            files={this.state.files}
                                            allowMultiple={true}
                                            labelIdle='Arraste e Solte aqui os arquivos, ou <span class="filepond--label-action">Escolha os Arquivos</span>'
                                            onupdatefiles={fileItems => {
                                                this.setState( {
                                                    files: fileItems.map( fileItem => fileItem.file )
                                                } );
                                            }}
                                        />
                                    </div>
                                </GridItem>
                            </GridContainer>
                            <Button
                                color="rose"
                                onClick={this.salvar}
                                className={classes.registerButton}>
                                Salvar
                                </Button>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer >
        );
    }
};

RoutinesFormComponent.propTypes = { classes: PropTypes.object };
export default withStyles( {
    infoText: { fontWeight: "300", margin: "10px 0 30px", textAlign: "center" },
    inputAdornmentIcon: { color: "#555" },
    inputAdornment: { position: "relative" }
} )( RoutinesFormComponent );