import React from "react";

import ReactTable from "react-table";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import SweetAlert from "react-bootstrap-sweetalert";
import Checkbox from "@material-ui/core/Checkbox";

import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardText from "components/Card/CardText.js";
import CardBody from "components/Card/CardBody.js";
import Check from "@material-ui/icons/Check";

import Api from '../../../services/Api';

import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import styles from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.js";

import { verifyEmail } from "services/validations";
import { formatPhone } from "services/utils";


class ClientsFormComponent extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            id: '',
            name: '',
            codigoClientePC: '',
            email: '',
            telefone: '',
            checkAtivo: false,
            checkAMS: false,
            isUpdate: false,
            inputNameState: '',
            inputCodigoClientePCState: '',
            inputTelefoneState: '',
            inputEmailState: '',
            alert: null,
            rotinasDoCliente: [],
            todasRotinas: []
        };
    }

    async componentDidMount() {

        const { id } = this.props.match.params;

        if ( id !== ":id" && id !== undefined ) {
            const result = await Api.get( `/clientes/?idCliente=${ id }` );
            this.setState( {
                id: id,
                name: result.data.nomeCliente,
                codigoClientePC: result.data.codigoClientePC,
                telefone: formatPhone( result.data.telefone ),
                email: result.data.email,
                checkAtivo: result.data.isAtivo,
                checkAMS: result.data.isAMS,
                isUpdate: true,
                rotinasDoCliente: result.data.rotinas
            } );
        }

        const resultRotinas = await Api.get( `/rotinas/?ativas=true` );

        resultRotinas.data.forEach( ( id, index ) => {
            this.setState( prevState => ( {
                todasRotinas: [...prevState.todasRotinas,
                {
                    ...resultRotinas.data[index],
                    selected: this.hasToCheck( resultRotinas.data[index]._id )
                }]
            } ) );
        } );
    }

    onChange = ( e ) => {
        this.setState( { [e.target.name]: e.target.value } );
    };

    setInputNameState = ( valueStatus ) => {
        this.setState( {
            inputNameState: valueStatus
        } );
    };

    setInputCodigoClientePCState = ( valueStatus ) => {
        this.setState( {
            inputCodigoClientePCState: valueStatus
        } );
    };

    setInputTelefoneState = ( valueStatus ) => {
        this.setState( {
            inputTelefoneState: valueStatus
        } );
    };

    setInputEmailState = ( valueStatus ) => {
        this.setState( {
            inputEmailState: valueStatus
        } );
    };

    salvar = async () => {
        try {
            if ( this.state.name.trim().length < 4 ) {
                this.openAlert( { redirect: false, message: 'Nome inválido, digite pelo menos 4 letras!' } );
                this.setInputNameState( "error" );
                return;
            }

            // if ( this.state.codigoClientePC.trim().length <= 0 || this.state.codigoClientePC.trim().length > 8 ) {
            //     this.openAlert( { redirect: false, message: 'Código do Cliente PC inválido, digite entre 1 e 8 dígitos!' } );
            //     this.setInputCodigoClientePCState( "error" );
            //     return;
            // }

            if ( this.state.codigoClientePC === '0' ) {
                this.openAlert( { redirect: false, message: 'Código do Cliente inválido!' } );
                this.setInputCodigoClientePCState( "error" );
                return;
            }

            if ( !this.state.telefone ) {
                this.openAlert( { redirect: false, message: 'Insira o Telefone!' } );
                this.setInputTelefoneState( "error" );
                return;
            }

            if ( this.state.telefone.trim().length < 14 ) {
                this.openAlert( { redirect: false, message: 'Telefone inválido!' } );
                this.setInputTelefoneState( "error" );
                return;
            }

            if ( !this.state.email ) {
                this.openAlert( { redirect: false, message: 'Insira o E-mail!' } );
                this.setInputEmailState( "error" );
                return;
            }

            if ( this.state.inputEmailState === "error" ) {
                this.openAlert( { redirect: false, message: 'E-mail inválido!' } );
                return;
            }

            this.setInputNameState( "success" );
            this.setInputCodigoClientePCState( "success" );
            this.setInputTelefoneState( "success" );
            this.setInputEmailState( "success" );

            var rotinasSelecionadas = [];
            this.state.todasRotinas.forEach( ( id, index ) => {
                if ( this.state.todasRotinas[index].selected === true )
                    rotinasSelecionadas.push( this.state.todasRotinas[index]._id );
            } );

            if ( this.state.isUpdate ) {
                await Api.put( `/clientes`,
                    {
                        id: this.state.id,
                        nomeCliente: this.state.name,
                        codigoClientePC: this.state.codigoClientePC,
                        email: this.state.email,
                        telefone: this.state.telefone.replace( /\D/g, '' ),
                        rotinas: rotinasSelecionadas,
                        isAtivo: this.state.checkAtivo,
                        isAMS: this.state.checkAMS
                    } );
            } else {
                await Api.post( `/clientes`,
                    {
                        nomeCliente: this.state.name,
                        codigoClientePC: this.state.codigoClientePC,
                        email: this.state.email,
                        telefone: this.state.telefone.replace( /\D/g, '' ),
                        rotinas: rotinasSelecionadas,
                        isAtivo: this.state.checkAtivo,
                        isAMS: this.state.checkAMS
                    } );
            }

            setTimeout( () => { this.props.history.push( '/admin/clientes' ); }, 2000 );
            this.openAlert( { redirect: true, message: 'Cadastro salvo com sucesso!' } );

        } catch ( err ) {
            if ( err.response )
                this.openAlert( { redirect: false, message: err.response.data.msg } );
            else
                this.openAlert( { redirect: false, message: err } );
        }
    };

    toggleCheckbox( event ) {
        switch ( event.target.value ) {
            case "checkAtivo":
                this.setState( {
                    checkAtivo: ( this.state.checkAtivo === "on" || this.state.checkAtivo === true ) ? false : true
                } );
                break;
            case "checkAMS":
                this.setState( {
                    checkAMS: ( this.state.checkAMS === "on" || this.state.checkAMS === true ) ? false : true
                } );
                break;

            default:

                //Marcou uma das checkboxes de rotina
                this.state.todasRotinas.forEach( ( id, index ) => {
                    if ( this.state.todasRotinas[index]._id === event.target.value ) {
                        const newIds = this.state.todasRotinas.slice(); //copy the array
                        newIds[index].selected = !newIds[index].selected; //execute the manipulations
                        this.setState( { todasRotinas: newIds } ); //set the new state
                    }
                } );
                break;
        }
    }

    hasToCheck( rotinaId ) {
        return this.state.rotinasDoCliente.find( element => element === rotinaId ) === undefined ? false : true;
    }

    setAlert( arg ) {
        this.setState( { alert: arg } );
    }

    hideAlert = () => {
        this.setAlert( null );
    };

    openAlert = ( { redirect, message } ) => {

        if ( redirect ) {
            this.setAlert(
                <SweetAlert
                    success
                    style={{ display: "block", marginTop: "-200px" }}
                    showCancel={false}
                    title={message}
                    confirmBtnText={''}
                    onConfirm={async () => {
                        this.setAlert( null );
                    }}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
                </SweetAlert> );
        } else {
            this.setAlert(
                <SweetAlert
                    danger
                    style={{ display: "block", marginTop: "-200px" }}
                    showCancel={false}
                    title={message}
                    confirmBtnText={'Certo!'}
                    onConfirm={async () => {
                        this.setAlert( null );
                    }}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
                </SweetAlert> );
        }
    };

    render() {

        const classes = makeStyles( styles );

        return (
            <GridContainer>
                <GridItem md={12}>
                    {/* CX. DIALOGO */}
                    <div>{this.state.alert}</div>
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose" text>
                            <CardText color="rose">
                                {this.state.isUpdate ?
                                    <h4 className={classes.cardTitle}>Edição de Cliente</h4>
                                    :
                                    <h4 className={classes.cardTitle}>Cadastro de Cliente</h4>
                                }
                            </CardText>
                        </CardHeader>
                        <CardBody>
                            <form>
                                <GridContainer>
                                    <GridItem xs={12} sm={10} md={6}>
                                        <legend>Dados do Cliente</legend>
                                        <CustomInput
                                            id="name"
                                            name="name"
                                            labelText={<span>Nome: <small>(obrigatório)</small></span>}
                                            success={this.state.inputNameState === "success"}
                                            error={this.state.inputNameState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                value: this.state.name,
                                                onChange: event => {
                                                    if ( event.target.value.trim().length >= 4 ) {
                                                        this.setInputNameState( "success" );
                                                    } else {
                                                        this.setInputNameState( "error" );
                                                    }
                                                    this.setState( {
                                                        name: event.target.value
                                                    } );
                                                },
                                                type: "text"
                                            }}
                                        />
                                        <CustomInput
                                            id="codigoClientePC"
                                            name="codigoClientePC"
                                            labelText={<span>Código do Cliente PC: <small>(obrigatório)</small></span>}
                                            success={this.state.inputCodigoClientePCState === "success"}
                                            error={this.state.inputCodigoClientePCState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                value: this.state.codigoClientePC,
                                                onChange: event => {
                                                    var codigo = event.target.value.replace( /\D/g, "" ).replace( '0', '' );

                                                    if ( codigo.toString().length > 0 && codigo.toString().length <= 8 && codigo > 0 ) {
                                                        this.setInputCodigoClientePCState( "success" );
                                                    } else {
                                                        this.setInputCodigoClientePCState( "error" );
                                                    }
                                                    this.setState( {
                                                        codigoClientePC: codigo.toString()
                                                    } );
                                                },
                                                type: "number"
                                            }}
                                        />
                                        <CustomInput
                                            id="telefone"
                                            name="telefone"
                                            labelText={<span>Telefone: <small>(obrigatório)</small></span>}
                                            success={this.state.inputTelefoneState === "success"}
                                            error={this.state.inputTelefoneState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                value: this.state.telefone,
                                                onChange: event => {
                                                    if ( event.target.value.trim().length >= 14 ) {
                                                        this.setInputTelefoneState( "success" );
                                                    } else {
                                                        this.setInputTelefoneState( "error" );
                                                    }
                                                    this.setState( {
                                                        telefone: formatPhone( event.target.value )
                                                    } );
                                                },
                                                type: "text"
                                            }}
                                        />
                                        <CustomInput
                                            id="email"
                                            name="email"
                                            labelText={<span>E-mail: <small>(obrigatório)</small></span>}
                                            success={this.state.inputEmailState === "success"}
                                            error={this.state.inputEmailState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                value: this.state.email,
                                                onChange: event => {
                                                    if ( verifyEmail( event.target.value ) ) {
                                                        this.setInputEmailState( "success" );
                                                    } else {
                                                        this.setInputEmailState( "error" );
                                                    }
                                                    this.setState( {
                                                        email: event.target.value
                                                    } );
                                                },
                                                type: "email",
                                                disabled: this.state.isUpdate
                                            }}
                                        />
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <legend>Controle do Cliente</legend>
                                        <div className={classes.block}>
                                            <FormControlLabel
                                                label="Cliente está Ativo?"
                                                control={
                                                    <Switch
                                                        checked={this.state.checkAtivo}
                                                        onChange={this.toggleCheckbox.bind( this )}
                                                        value="checkAtivo"
                                                        classes={{
                                                            switchBase: classes.switchBase,
                                                            checked: classes.switchChecked,
                                                            thumb: classes.switchIcon,
                                                            track: classes.switchBar
                                                        }}
                                                    />
                                                }
                                                classes={{
                                                    label: classes.label
                                                }}
                                            />
                                            <br />
                                            <FormControlLabel
                                                label="Cliente AMS (com direito a todas as Rotinas)"
                                                control={
                                                    <Switch
                                                        checked={this.state.checkAMS}
                                                        onChange={this.toggleCheckbox.bind( this )}
                                                        value="checkAMS"
                                                        classes={{
                                                            switchBase: classes.switchBase,
                                                            checked: classes.switchChecked,
                                                            thumb: classes.switchIcon,
                                                            track: classes.switchBar
                                                        }}
                                                    />
                                                }
                                                classes={{
                                                    label: classes.label
                                                }}
                                            />
                                        </div>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={12}>
                                        <legend>Rotinas</legend>
                                        <ReactTable
                                            data={
                                                this.state.todasRotinas.map( rotina => {
                                                    return ( {
                                                        id: rotina._id,
                                                        nome: rotina.nomeRotina,
                                                        descricao: rotina.descricaoRotina,
                                                        actions: (
                                                            <div className={classes.checkboxAndRadio}>
                                                                <FormControlLabel
                                                                    control={
                                                                        <Checkbox
                                                                            checkedIcon={<Check className={classes.checkedIcon} />}
                                                                            icon={<Check className={classes.uncheckedIcon} />}
                                                                            onChange={this.toggleCheckbox.bind( this )}
                                                                            value={rotina._id}
                                                                            defaultChecked={this.hasToCheck( rotina._id )}
                                                                            classes={{
                                                                                checked: classes.checked,
                                                                                root: classes.checkRoot
                                                                            }}
                                                                        />
                                                                    }
                                                                    classes={{
                                                                        label: classes.label,
                                                                        root: classes.labelRoot
                                                                    }}
                                                                />
                                                            </div> )
                                                    } );
                                                } )
                                            }
                                            columns={[
                                                {
                                                    Header: "",
                                                    accessor: "actions",
                                                    sortable: false,
                                                    filterable: false,
                                                    resizable: false,
                                                    width: 50
                                                }, {
                                                    Header: "Nome",
                                                    accessor: "nome",
                                                    sortable: true,
                                                    filterable: true,
                                                    resizable: true,
                                                    width: 150
                                                },
                                                {
                                                    Header: "Descrição",
                                                    accessor: "descricao",
                                                    sortable: true,
                                                    filterable: true,
                                                    resizable: false
                                                },
                                                {
                                                    Header: "",
                                                    accessor: "a",
                                                    sortable: false,
                                                    filterable: false,
                                                    resizable: false,
                                                    width: 1
                                                }
                                            ]}
                                            defaultPageSize={( this.state.todasRotinas.length === 0 ) ? 5 : this.state.todasRotinas.length}
                                            minRows={1}
                                            showPaginationTop
                                            showPaginationBottom={false}
                                            nextText={'Próximo'}
                                            previousText={'Anterior'}
                                            rowsText={'rotinas'}
                                            pageText={'Página'}
                                            ofText={'de'}
                                            loadingText='Carregando...'
                                            noDataText='Nenhuma Rotina encontrada'
                                            className="-striped -highlight"
                                        />
                                    </GridItem>
                                </GridContainer>
                                <br />
                                <br />
                                <Button
                                    color="rose"
                                    onClick={this.salvar}
                                    className={classes.registerButton}>
                                    Salvar
                                </Button>
                            </form>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        );
    }
}

ClientsFormComponent.propTypes = { classes: PropTypes.object };
export default withStyles( {
    infoText: { fontWeight: "300", margin: "10px 0 30px", textAlign: "center" },
    inputAdornmentIcon: { color: "#555" },
    inputAdornment: { position: "relative" }
} )( ClientsFormComponent );;