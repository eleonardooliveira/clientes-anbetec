import React from 'react';

import ReactTable from "react-table";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import SweetAlert from "react-bootstrap-sweetalert";

// @material-ui/icons
import Assignment from "@material-ui/icons/Assignment";
import Close from "@material-ui/icons/Close";

import Api from '../../../services/Api';

// core components
import GridContainer from "components/Grid/GridContainer.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import GridItem from "components/Grid/GridItem.js";
import CardBody from "components/Card/CardBody.js";
import CardIcon from "components/Card/CardIcon.js";
import CardHeader from "components/Card/CardHeader.js";

import { Link } from 'react-router-dom';

import { cardTitle } from "assets/jss/material-dashboard-pro-react.js";

const styles = {
    cardIconTitle: {
        ...cardTitle,
        marginTop: "15px",
        marginBottom: "0px"
    }
};

const useStyles = makeStyles( styles );

export default function ClientsReactTable( props ) {
    const [clientes, setClientes] = React.useState( props.clientes );
    const [alert, setAlert] = React.useState( null );
    React.useEffect( () => setClientes( props.clientes ), [props.clientes] );

    const classes = useStyles();

    const openAlert = ( { type, cliente } ) => {
        if ( type === 'delete' )
            setAlert(
                <SweetAlert
                    danger
                    showCancel={true}
                    title="Confirmação"
                    confirmBtnText={'Confirmar'}
                    cancelBtnText={'Cancelar'}
                    onConfirm={async () => {

                        var newClientes = clientes;
                        for ( var i = 0; i < clientes.length; i++ ) {

                            if ( clientes[i]._id === cliente._id ) {
                                //excluindo do banco
                                const res = await Api.delete( `/clientes/${ cliente._id }` );

                                if ( res.status === 200 ) {
                                    //excluindo da tela
                                    newClientes.splice( i, 1 );
                                    setClientes( [...newClientes] );
                                }
                                break;
                            }

                        }
                        setAlert( null );
                    }}
                    onCancel={() => {
                        setAlert( null );
                    }}
                    cancelBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 MuiButton-text`}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>
                    Deseja realmente excluir o Cliente '<b>{cliente.nomeCliente}</b>'' ?
          </SweetAlert > );

    };

    return (
        <GridContainer>
            <GridItem md={12}>
                {/* CX. DIALOGO */}
                <div>{alert}</div>
            </GridItem>
            <GridItem xs={12}>
                <div style={{ display: "flex" }}>
                    <Button
                        xs={3}
                        style={{ marginLeft: "auto" }}
                        color="info"
                        onClick={() => {
                            props.history.push( '/admin/cliente' );
                        }}>
                        Cadastrar Cliente
                     </Button>
                </div>
                <Card>
                    <CardHeader color="primary" icon>
                        <CardIcon color="primary">
                            <Assignment />
                        </CardIcon>
                        <h4 className={classes.cardIconTitle}>Clientes</h4>
                    </CardHeader>
                    <CardBody>
                        <ReactTable
                            data={
                                clientes.map( cliente => {
                                    return ( {
                                        id: cliente._id,
                                        nome: cliente.nomeCliente,
                                        ativo: cliente.isAtivo === true ? "Sim" : "Não",
                                        ams: cliente.isAMS === true ? "Sim" : "Não",
                                        actions: (
                                            <div className="actions-right">
                                                <Link to={`/admin/cliente/${ cliente._id }`} className="btn btn-pink" >Editar</Link>
                                                {/* use this button to remove the data row */}
                                                <Button
                                                    justIcon
                                                    round
                                                    simple
                                                    onClick={() => { openAlert( { type: 'delete', cliente: cliente } ); }}
                                                    color="danger"
                                                    className="remove"
                                                >
                                                    <Close />
                                                </Button>{" "}</div>
                                        )
                                    } );
                                } )
                            }
                            columns={[
                                {
                                    Header: "Nome",
                                    accessor: "nome",
                                    sortable: true,
                                    filterable: true,
                                    resizable: true
                                },
                                {
                                    Header: "Ativo",
                                    accessor: "ativo",
                                    sortable: true,
                                    filterable: true,
                                    resizable: false
                                },
                                {
                                    Header: "AMS",
                                    accessor: "ams",
                                    sortable: true,
                                    filterable: true,
                                    resizable: false
                                },
                                {
                                    Header: "Ações",
                                    accessor: "actions",
                                    sortable: false,
                                    filterable: false,
                                    resizable: false
                                }
                            ]}
                            defaultPageSize={( clientes.length === 0 ) ? 5 : clientes.length}
                            minRows={1}
                            showPaginationTop
                            showPaginationBottom={false}
                            nextText={'Próximo'}
                            previousText={'Anterior'}
                            rowsText={'clientes'}
                            pageText={'Página'}
                            ofText={'de'}
                            loadingText='Carregando...'
                            noDataText='Nenhum Cliente encontrado'
                            className="-striped -highlight"
                        />

                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer >
    );
};