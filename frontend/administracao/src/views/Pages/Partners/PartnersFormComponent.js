import React from "react";

import ReactTable from "react-table";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import SweetAlert from "react-bootstrap-sweetalert";

import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardText from "components/Card/CardText.js";
import CardBody from "components/Card/CardBody.js";

import Api from '../../../services/Api';

import { Link } from 'react-router-dom';

import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import styles from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.js";
import { formatDate } from "services/utils";

import { verifyEmail } from "services/validations";
import { formatPhone } from "services/utils";

class PartnersFormComponent extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            id: '',
            name: '',
            email: '',
            telefone: '',
            dataRegistro: '',
            checkAtivo: true,
            isUpdate: false,
            inputNameState: '',
            alert: null,
            todasRotinas: []
        };
    }

    async componentDidMount() {
        const { id } = this.props.match.params;

        if ( id !== ":id" && id !== undefined ) {
            const result = await Api.get( `/parceiros/?idParceiro=${ id }` );

            this.setState( {
                id: id,
                name: result.data.nomeParceiro,
                telefone: formatPhone( result.data.telefone ),
                email: result.data.email,
                dataRegistro: result.data.data_registro,
                checkAtivo: result.data.isAtivo,
                isUpdate: true
            } );
        }

        const resultRotinas = await Api.get( `/rotinas/?idParceiro=${ id }` );

        this.setState( { todasRotinas: resultRotinas.data } );
    }

    setAlert( arg ) {
        this.setState( { alert: arg } );
    }

    onChange = ( e ) => {
        this.setState( { [e.target.name]: e.target.value } );
    };

    setInputNameState = ( valueStatus ) => {
        this.setState( {
            inputNameState: valueStatus
        } );

    };

    setInputTelefoneState = ( valueStatus ) => {
        this.setState( {
            inputTelefoneState: valueStatus
        } );
    };

    setInputEmailState = ( valueStatus ) => {
        this.setState( {
            inputEmailState: valueStatus
        } );
    };

    salvar = async () => {

        try {

            if ( this.state.name.trim().length < 6 ) {
                this.openAlert( { redirect: false, message: 'Nome inválido, digite pelo menos 6 letras!' } );
                this.setInputNameState( "error" );
                return;
            }

            if ( !this.state.telefone ) {
                this.openAlert( { redirect: false, message: 'Insira o Telefone!' } );
                this.setInputTelefoneState( "error" );
                return;
            }

            if ( this.state.telefone.trim().length < 14 ) {
                this.openAlert( { redirect: false, message: 'Telefone inválido!' } );
                this.setInputTelefoneState( "error" );
                return;
            }

            if ( !this.state.email ) {
                this.openAlert( { redirect: false, message: 'Insira o E-mail!' } );
                this.setInputEmailState( "error" );
                return;
            }

            if ( this.state.inputEmailState === "error" ) {
                this.openAlert( { redirect: false, message: 'E-mail inválido!' } );
                return;
            }

            this.setInputNameState( "success" );
            this.setInputTelefoneState( "success" );
            this.setInputEmailState( "success" );

            if ( this.state.isUpdate ) {
                await Api.put( `/parceiros`,
                    {
                        id: this.state.id,
                        nomeParceiro: this.state.name,
                        telefone: this.state.telefone.replace( /\D/g, '' ),
                        isAtivo: this.state.checkAtivo
                    } );
            } else {
                await Api.post( `/parceiros`,
                    {
                        nomeParceiro: this.state.name,
                        email: this.state.email,
                        telefone: this.state.telefone.replace( /\D/g, '' ),
                        isAtivo: this.state.checkAtivo
                    } );
            }

            setTimeout( () => { this.props.history.push( '/admin/parceiros' ); }, 2000 );
            this.openAlert( { redirect: true, message: 'Cadastro salvo com sucesso!' } );

        } catch ( err ) {
            if ( err.response )
                this.openAlert( { redirect: false, message: err.response.data.msg } );
            else
                this.openAlert( { redirect: false, message: err } );
        }
    };

    toggleCheckbox( event ) {
        switch ( event.target.value ) {
            case "checkAtivo":
                let newValue = ( this.state.checkAtivo === "on" || this.state.checkAtivo === true ) ? false : true;

                if ( newValue === false ) {//se está desativando, deve impossibilitar de acessar rotinas tm
                    this.setState( {
                        checkAtivo: false
                    } );
                } else {
                    this.setState( {
                        checkAtivo: true
                    } );
                }
                break;

            default:
                break;
        }
    }

    hideAlert = () => {
        this.setAlert( null );
    };

    openAlert = ( { redirect, message } ) => {
        if ( redirect ) {
            this.setAlert(
                <SweetAlert
                    success
                    style={{ display: "block", marginTop: "-200px" }}
                    showCancel={false}
                    title={message}
                    confirmBtnText={''}
                    onConfirm={async () => {
                        this.setAlert( null );
                    }}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>

                </SweetAlert> );
        } else {
            this.setAlert(
                <SweetAlert
                    danger
                    style={{ display: "block", marginTop: "-200px" }}
                    showCancel={false}
                    title={message}
                    confirmBtnText={'Certo!'}
                    onConfirm={async () => {
                        this.setAlert( null );
                    }}
                    confirmBtnCssClass={`MuiButtonBase-root MuiButton-root makeStyles-button-173 makeStyles-sm-197 makeStyles-info-176 MuiButton-text`}>

                </SweetAlert> );
        }
    };

    render() {

        const classes = makeStyles( styles );

        return (
            <GridContainer>
                <GridItem md={12}>
                    {/* CX. DIALOGO */}
                    <div>{this.state.alert}</div>
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose" text>
                            <CardText color="rose">
                                {this.state.isUpdate ?
                                    <h4 className={classes.cardTitle}>Edição de Parceiro</h4>
                                    :
                                    <h4 className={classes.cardTitle}>Cadastro de Parceiro</h4>
                                }
                            </CardText>
                        </CardHeader>
                        <CardBody>
                            <form>
                                <GridContainer>
                                    <GridItem xs={12} sm={10} md={6}>
                                        <legend>Dados do Parceiro</legend>
                                        <CustomInput
                                            id="name"
                                            name="name"
                                            labelText={<span>Nome: <small>(obrigatório)</small></span>}
                                            success={this.state.inputNameState === "success"}
                                            error={this.state.inputNameState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                placeholder: "Nome",
                                                value: this.state.name,
                                                onChange: event => {
                                                    if ( event.target.value.trim().length >= 6 ) {
                                                        this.setInputNameState( "success" );
                                                    } else {
                                                        this.setInputNameState( "error" );
                                                    }
                                                    this.setState( {
                                                        name: event.target.value
                                                    } );
                                                },
                                                type: "text"
                                            }}
                                        />
                                        <CustomInput
                                            id="telefone"
                                            name="telefone"
                                            labelText={<span>Telefone: <small>(obrigatório)</small></span>}
                                            success={this.state.inputTelefoneState === "success"}
                                            error={this.state.inputTelefoneState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                value: this.state.telefone,
                                                onChange: event => {
                                                    if ( event.target.value.trim().length >= 14 ) {
                                                        this.setInputTelefoneState( "success" );
                                                    } else {
                                                        this.setInputTelefoneState( "error" );
                                                    }
                                                    this.setState( {
                                                        telefone: formatPhone( event.target.value )
                                                    } );
                                                },
                                                type: "text"
                                            }}
                                        />
                                        <CustomInput
                                            id="email"
                                            name="email"
                                            labelText={<span>E-mail: <small>(obrigatório)</small></span>}
                                            success={this.state.inputEmailState === "success"}
                                            error={this.state.inputEmailState === "error"}
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                value: this.state.email,
                                                onChange: event => {
                                                    if ( verifyEmail( event.target.value ) ) {
                                                        this.setInputEmailState( "success" );
                                                    } else {
                                                        this.setInputEmailState( "error" );
                                                    }
                                                    this.setState( {
                                                        email: event.target.value
                                                    } );
                                                },
                                                type: "email",
                                                disabled: this.state.isUpdate
                                            }}
                                        />
                                        {this.state.isUpdate ?
                                            <CustomInput
                                                id="data_registro"
                                                name="data_registro"
                                                labelText="Data de Registro"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    value: formatDate( this.state.dataRegistro ),
                                                    type: "text",
                                                    disabled: true
                                                }}
                                            />
                                            :
                                            null
                                        }

                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <legend>Controle do Parceiro</legend>
                                        <div className={classes.block}>
                                            <FormControlLabel
                                                label="Parceiro está Ativo?"
                                                control={
                                                    <Switch
                                                        checked={this.state.checkAtivo}
                                                        onChange={this.toggleCheckbox.bind( this )}
                                                        value="checkAtivo"
                                                        classes={{
                                                            switchBase: classes.switchBase,
                                                            checked: classes.switchChecked,
                                                            thumb: classes.switchIcon,
                                                            track: classes.switchBar
                                                        }}
                                                    />
                                                }
                                                classes={{
                                                    label: classes.label
                                                }}
                                            />
                                        </div>
                                    </GridItem>
                                    {this.state.isUpdate ?
                                        < GridItem xs={12} sm={12} md={12}>
                                            <legend>Rotinas do Parceiro</legend>
                                            <ReactTable
                                                data={
                                                    this.state.todasRotinas.map( rotina => {
                                                        return ( {
                                                            id: rotina._id,
                                                            nome: rotina.nomeRotina,
                                                            descricao: rotina.descricaoRotina,
                                                            actions: (
                                                                <div className="actions-right">
                                                                    <Link to={`/admin/rotina/${ rotina._id }`} className="btn btn-pink" >Visualizar Rotina</Link></div>
                                                            )
                                                        } );
                                                    } )
                                                }
                                                columns={[{
                                                    Header: "Nome",
                                                    accessor: "nome",
                                                    sortable: true,
                                                    filterable: true,
                                                    resizable: true,
                                                    width: 150
                                                },
                                                {
                                                    Header: "Descrição",
                                                    accessor: "descricao",
                                                    sortable: true,
                                                    filterable: true,
                                                    resizable: false
                                                },
                                                {
                                                    Header: "Ações",
                                                    accessor: "actions",
                                                    sortable: false,
                                                    filterable: false,
                                                    resizable: false
                                                }
                                                ]}
                                                defaultPageSize={( this.state.todasRotinas.length === 0 ) ? 5 : this.state.todasRotinas.length}
                                                minRows={1}
                                                showPaginationTop
                                                showPaginationBottom={false}
                                                nextText={'Próximo'}
                                                previousText={'Anterior'}
                                                rowsText={'rotinas'}
                                                pageText={'Página'}
                                                ofText={'de'}
                                                loadingText='Carregando...'
                                                noDataText='Nenhuma Rotina encontrada'
                                                className="-striped -highlight"
                                            />
                                        </ GridItem>
                                        :
                                        null
                                    }
                                </GridContainer>
                                <br />
                                <br />
                                <Button
                                    color="rose"
                                    onClick={this.salvar}
                                    className={classes.registerButton}>
                                    Salvar
                                </Button>
                            </form>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer >
        );
    }
}

PartnersFormComponent.propTypes = { classes: PropTypes.object };
export default withStyles( {
    infoText: { fontWeight: "300", margin: "10px 0 30px", textAlign: "center" },
    inputAdornmentIcon: { color: "#555" },
    inputAdornment: { position: "relative" }
} )( PartnersFormComponent );;