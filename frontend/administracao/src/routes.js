import LoginPage from "views/Pages/LoginPage";
import ClientsTableComponent from "views/Pages/Clients/ClientsTableComponent";
import ClientsFormComponent from "views/Pages/Clients/ClientsFormComponent";
import RoutinesTableComponent from "views/Pages/Routines/RoutinesTableComponent";
import RoutinesFormComponent from "views/Pages/Routines/RoutinesFormComponent";
import PartnersFormComponent from "views/Pages/Partners/PartnersFormComponent";
import PartnersTableComponent from "views/Pages/Partners/PartnersTableComponent";

var dashRoutes = [
  {//Tela de Login de Parceiro
    path: "/login-page/:deslogado?",
    name: "Login",
    component: LoginPage,
    layout: "/auth"
  },
  {//Componente Tabela dos Clientes
    path: "/clientes",
    name: "Clientes",
    mini: "CLI",
    component: ClientsTableComponent,
    layout: "/admin"
  },
  {//Formulário de Cadastro/Edição de Clientes
    path: "/cliente/:id?",
    name: "Cliente",
    mini: "NC",
    component: ClientsFormComponent,
    layout: "/admin"
  },
  {//Componente Tabela das Rotinas
    path: "/rotinas",
    name: "Rotinas",
    mini: "ROT",
    component: RoutinesTableComponent,
    layout: "/admin"
  },
  {//Formulário de Cadastro/Edição de Rotinas
    path: "/rotina/:id?",
    name: "Rotina",
    mini: "R",
    component: RoutinesFormComponent,
    layout: "/admin"
  },
  {//Componente Tabela dos Parceiros
    path: "/parceiros",
    name: "Parceiros",
    mini: "PAR",
    component: PartnersTableComponent,
    layout: "/admin"
  },
  {//Formulário de Cadastro/Edição de Parceiros
    path: "/parceiro/:id?",
    name: "Parceiro",
    mini: "P",
    component: PartnersFormComponent,
    layout: "/admin"
  }
];
export default dashRoutes;
