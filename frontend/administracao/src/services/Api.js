import axios from 'axios';
import { getToken } from "./AuthService";

const Api = axios.create( { baseURL: 'http://localhost:7000/api' } );

Api.interceptors.request.use( async config => {
    const token = getToken();
    if ( token ) {
        config.headers['x-auth-token'] = token;
    }

    config.headers['Content-type'] = 'application/json';

    return config;
} );

export default Api; 